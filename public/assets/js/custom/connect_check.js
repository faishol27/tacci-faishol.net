var socket = io();
var sttConnectionNotif = false;

socket.on('connect_error', () => {
	if(sttConnectionNotif == false && $()){
		$.notify({
			icon: 'fa fa-exclamation-triangle',
			message: `You are offline. Some functionality maybe unavailable.` 
		}, {
			placement: {
				from: "top",
				align: "center"
			},
			type: "warning"
		});

		if($('#con_status').length > 0){
			var con_stt = $('#con_status').children();
			con_stt[0].style.color = '#a94442';
			con_stt[1].innerHTML = "Offline";
		}

		if($('.overlay').length == 0){
			$('.box').append("<div class='overlay' id='offline'></div>");
		}
		
		$('button').attr('disabled', 'true');
		sttConnectionNotif = true;
	}
});

socket.on('connect', () => {
	sttConnectionNotif = false;
	
	if($('#con_status').length > 0){
		var con_stt = $('#con_status').children();
		con_stt[0].style.color = '#3c763d';
		con_stt[1].innerHTML = "Online";
	}

	if($('.box #offline').length > 0){
		$('.box #offline').remove();
	}
	
	$('button').removeAttr('disabled');
});