/*					
 * Global Variables
 */
var game_master = io.connect('/game-master')
var game_client = io.connect('/game-client')
var gameJSON
var gameSettings
var resetLastBox = false

var active_box
var gameProblem

var bg_teamBoard = ["bg-blue", "bg-red", "bg-yellow", "bg-green", "bg-purple", "bg-orange"]
var	len_bgBoard = bg_teamBoard.length

var boxType = ["Empty", "Ambil Alih", "Double Poin", "Extra Box", "Sedekah Poin", "Sedekah Box", "Skipped"]

$(".sortable").sortable({
	connectWith: ".connectedSortable"
});

$("#label_main-timer")
	.countdown(Date.now() + 2, {precision: 10})
	.on("update.countdown", function(event) {
		$(this).text( event.strftime('%H : %M : %S') );
	})
	.on("finish.countdown", function(event){
		$(this).text( event.strftime('%H : %M : %S') );

		var tmpJSON = JSON.parse(JSON.stringify(gameJSON));
		tmpJSON.timer.main.end = -1;
		tmpJSON.timer.main.left = 0;
		
		var tmpLog = {
			user_id: 0,
			description: `Time's up!`,
			json_data: JSON.stringify(tmpJSON)
		};

		game_master.emit("action", tmpLog, (err) => {
			if(err) error_notif(`Whoops! Something went wrong`);
		});
	})
	.countdown('stop');

$("#detail_timer-label")
	.countdown(Date.now() + 2, {precision: 0, showMiliseconds: true})
	.on("update.countdown", function(event) {
		$(this).text( event.strftime('%S s') );
	})
	.on("finish.countdown", function(event){
		$(this).text( event.strftime('%S s') );
		
		var tmpJSON = JSON.parse(JSON.stringify(gameJSON));
		tmpJSON.timer.problem.end = -1;
		tmpJSON.timer.problem.left = 0;
		
		if(gameJSON.box.slide[active_box] === 3){
			tmpJSON.box.slide[active_box] = 2
		}

		var tmpLog = {
			user_id: 0,
			description: `Time to answer's up!`,
			json_data: JSON.stringify(tmpJSON)
		};

		game_master.emit("action", tmpLog, (err) => {
			if(err) error_notif(`Whoops! Something went wrong [${err}]`);
		});
	})
	.countdown('stop');	

/*
 * Global Function
 */

function error_notif(msg){
	$.notify({
		icon: 'fa fa-exclamation-triangle',
		message: msg
	}, {
		placement: {
			from: "top",
			align: "center"
		},
		type: "warning"
	});
}

function success_notif(msg){
	$.notify({
		icon: 'fa fa-check',
		message: msg
	}, {
		placement: {
			from: "top",
			align: "center"
		},
		type: "success"
	});
}

function danger_notif(msg){
	$.notify({
		icon: 'fa fa-ban',
		message: msg 
	}, {
		placement: {
			from: "top",
			align: "center"
		},
		type: "danger"
	});
}

/*					
 * Controller
 */

async function initDetailPanel(newJSON){
	await $.ajax({
		url: `/api/v1/problem/${newJSON.problem_id}`,
		method: 'GET',
		success: function(data){
			gameProblem = data.response[0]
		}
	})

	$("#detail_box-label").html(active_box);

	if(newJSON.box.slide[active_box] <= 1){
		$("#detail_next-slide").removeAttr('disabled')
		$("#detail_btn-show-desc").attr('disabled', 'true')
	}else{
		$("#detail_next-slide").attr('disabled', 'true')
		$("#detail_btn-show-desc").removeAttr('disabled')
	}

	if(newJSON.box.slide[active_box] >= 1) $("#detail_prev-slide").removeAttr('disabled')
	else $("#detail_prev-slide").attr('disabled', 'true')

	if(newJSON.box.slide[active_box] <= 2){
		$('#detail_btn-show-desc').css('display', 'inline-block')
		$('#detail_btn-hide-desc').css('display', 'none')
	}else if(newJSON.box.slide[active_box] === 3){
		$('#detail_btn-show-desc').css('display', 'none')
		$('#detail_btn-hide-desc').css('display', 'inline-block')
	}

	$(".shape #side-0 div div").html(active_box)
	$(".shape #side-1 div div").html(boxType[gameProblem.type])
	
	if(newJSON.timer.problem.end !== -1){
		startProblemCountdown(newJSON.timer.problem.end)
	}else if(newJSON.timer.problem.left !== -1){
		stopProblemCountdown(newJSON.timer.problem.left)
	}
}

function initGame(oldJSON, newJSON){
	// Board controller
	var box_animate = newJSON.box.slide;
	var hideDetail = box_animate.every(v => v === -1);
	active_box = box_animate.findIndex(v => v > -1);
	
	if(!hideDetail){
		initDetailPanel(newJSON);
		$(".board-control").addClass("detail-control-show");

		var sideNow
		var sideNew = newJSON.box.slide[active_box]
		
		if(oldJSON) sideNow = oldJSON.box.slide[active_box]
		else sideNow = 0
		
		if(sideNew <= 2){
			$('.shape').shape('set next side', `.shape #side-${sideNew}`)
			
			if(sideNow < sideNew){
				$('.shape').shape('flip right')
			}else if(sideNow > sideNew){
				$('.shape').shape('flip left')
			}
		}
	}else{
		$(".board-control").removeClass("detail-control-show");
	}
	
	// Main timer controller
	if(newJSON.timer.main.left >= 0){
		resetMainLabelTimer(newJSON.timer.main.left);
	}else{
		startMainCountdown(newJSON.timer.main.end);
	}
	
	// Board box
	if(oldJSON){
		var oldBoxStatus = oldJSON.box.status
		var newBoxStatus = newJSON.box.status
		oldBoxStatus.forEach((v, idx) => {
			if(v !== newBoxStatus[idx]){
				if(v !== 0) $(`#box-${idx}`).removeClass(bg_teamBoard[(v - 1) % len_bgBoard])
				else if(v === 0) $(`#box-${idx}`).removeClass('bg-default')

				var newOwner = newBoxStatus[idx]
				
				if(newOwner === 0){
					$(`#box-${idx}`).addClass('bg-default')
					$(`#box-${idx} h1`).html(idx)
				}else{
					$(`#box-${idx}`).addClass(bg_teamBoard[(newOwner - 1) % len_bgBoard])
					$(`#box-${idx} h1`).html(String.fromCharCode(64 + newOwner))
				}
			}
		})
	}else{
		newJSON.box.status.forEach((v, idx) => {
			if(v !== 0){
				$(`#box-${idx}`).addClass(bg_teamBoard[(v - 1) % len_bgBoard])
				$(`#box-${idx} h1`).html(String.fromCharCode(64 + v))
			}else{
				$(`#box-${idx}`).addClass('bg-default')
				$(`#box-${idx} h1`).html(idx)
			}
		})
	}

	// Game turn controller
	
	var teamNow = newJSON.turn;
	var teamLastBox = newJSON.team[teamNow].last_boxType;
	
	if(oldJSON){
		var teamOld = oldJSON.turn;
		$(".turn-team").removeClass(bg_teamBoard[(teamOld - 1) % len_bgBoard])

		if(teamLastBox === oldJSON.team[teamNow].last_boxType && teamNow != teamOld){
			resetLastBox = true
		}
	}

	$(".turn-team").addClass(bg_teamBoard[(teamNow - 1) % len_bgBoard]);
	$("#turn_team-label").html(String.fromCharCode(64 + teamNow));
	$("#turn_last-box").html(boxType[teamLastBox]);
	
	// Team score controller
	if(oldJSON) updateScoreboard(newJSON.team, oldJSON.team);
	else updateScoreboard(newJSON.team);
}

game_master.on("problem", (data) => { gameProblem = data })

game_client.on("init", (err, data) => {
	if(err) error_notif(`Whoops! Something went wrong[2]`);
	else gameSettings = data;
});

game_client.on("signal", (data) => {
	if(data == "start"){
		$("#btn_start").css("display", "none");
		$("#btn_stop").css("display", "inline-block");
		
		$(".box #game_not_active").remove();
	}else{
		gameJSON = null

		$("#btn_start").css("display", "inline-block");
		$("#btn_stop").css("display", "none");
		
		if($(".overlay#game_not_active").length == 0){
			$(".box").append("<div class='overlay' id='game_not_active'></div>");
		}
	}
});

game_client.on("command", (data) => {
	resetLastBox = false
	initGame(gameJSON, JSON.parse(data.json_data));
	
	gameJSON = JSON.parse(data.json_data);

	var teamNow = gameJSON.turn
	if(resetLastBox === true){
		gameJSON.team[teamNow].last_boxType = 0
	}
});

/*
 * General
 */

function startGame(){
	game_master.emit("switch", "start", (err) => {
		if(err) error_notif(`Whoops! Cannot create a new game.`);
		else success_notif(`Successfully create a new game`);
	});
}

function stopGame(){
	game_master.emit("switch", "stop", (err) => {
		if(err) error_notif(`Sorry, failed to stopped the game.`);
		else success_notif(`Successfully stopped the game`);
	});
}

function confirmStopGame(){
	bootbox.confirm({ 
		size: "medium",
		title: "Confirmation",
		message: "Are you sure you want to stop the game?",
		buttons: {
			confirm: {
				label: 'Yes',
				className: 'btn btn-default'
			},
			cancel: {
				label: 'No',
				className: 'btn btn-primary'
			}
		},	
		callback: (result) => { if(result == true) stopGame(); }
	});
}

/*
 * Main Timer Panel
 */

function startMainCountdown(endTime){
	$("#btn_start-main-timer").css("display", "none");
	$("#btn_pause-main-timer").css("display", "inline-block");
	
	$("#label_main-timer").countdown(endTime);
}

function resetMainLabelTimer(time_left){
	$("#btn_start-main-timer").css("display", "inline-block");
	$("#btn_pause-main-timer").css("display", "none");
	$("#label_main-timer").countdown('stop');
	
	if(time_left == 0){
		$("#btn_start-main-timer").attr("disabled", "true");
		danger_notif(`Time is up!`)
	}else{
		$("#btn_start-main-timer").removeAttr("disabled");
	}

	var sec_left = Math.floor(time_left / 1000),
		hh = Math.floor(sec_left / 60 / 60).toString(),
		mm = Math.floor(sec_left / 60) % 60,
		ss = sec_left % 60;
	
	if(hh < 10) hh = "0" + hh.toString();
	if(mm < 10) mm = "0" + mm.toString();
	if(ss < 10) ss = "0" + ss.toString();

	$("#label_main-timer").html(`${hh} : ${mm} : ${ss}`);
}

function startMainTimer(){
	var tmpJSON = JSON.parse(JSON.stringify(gameJSON));
	tmpJSON.timer.main = {
		end: Date.now() + parseInt(gameJSON.timer.main.left),
		left: -1
	};

	var tmpLog = {
		user_id: 0,
		description: `Start main timer`,
		json_data: JSON.stringify(tmpJSON)
	};

	game_master.emit("action", tmpLog, (err) => {
		if(err) error_notif(`Whoops! Something went wrong.`);
	});
}

function pauseMainTimer(){
	var timeNow = Date.now();
	var timeLeft = gameJSON.timer.main.end - timeNow;

	var tmpJSON = JSON.parse(JSON.stringify(gameJSON));
	tmpJSON.timer.main = {
		end: -1,
		left: parseInt(timeLeft)
	};

	var tmpLog = {
		user_id: 0,
		description: `Pause main timer`,
		json_data: JSON.stringify(tmpJSON)
	};

	game_master.emit("action", tmpLog, (err) => {
		if(err) error_notif(`Whoops! Something went wrong.`);
	});
}

function resetMainTimer(){
	var tmpJSON = JSON.parse(JSON.stringify(gameJSON));
	tmpJSON.timer.main = {
		end: -1,
		left: parseInt(gameSettings.time_limit)
	};

	var tmpLog = {
		user_id: 0,
		description: `Reset main timer`,
		json_data: JSON.stringify(tmpJSON)
	};

	game_master.emit("action", tmpLog, (err) => {
		if(err) error_notif(`Whoops! Something went wrong.`);
	});
}

/*					
 * Turn Panel
 */
function doNextTurn(){
	var team_order = gameSettings.team_order.split(",");

	var oldTurnIdx = team_order.indexOf(String(gameJSON.turn));
	var newTurnIdx = (oldTurnIdx + 1) % team_order.length;
	
	var oldTurnLabel = String.fromCharCode(64 + gameJSON.turn);
	var newTurnLabel = String.fromCharCode(64 + parseInt(team_order[newTurnIdx]));

	var tmpJSON = JSON.parse(JSON.stringify(gameJSON));
	tmpJSON.turn = parseInt(team_order[newTurnIdx]);
	
	var tmpLog = {
		user_id: 0,
		description: `Change turn from team ${oldTurnLabel} to team ${newTurnLabel}`,
		json_data: JSON.stringify(tmpJSON)
	}

	game_master.emit("action", tmpLog, (err) => {
		if(err) error_notif(`Whoops! Something went wrong.`);
	});
}

function doPrevTurn(){
	var team_order = gameSettings.team_order.split(",");

	var oldTurnIdx = team_order.indexOf(String(gameJSON.turn));
	var newTurnIdx = (oldTurnIdx - 1 + team_order.length) % team_order.length;
	
	var oldTurnLabel = String.fromCharCode(64 + gameJSON.turn);
	var newTurnLabel = String.fromCharCode(64 + parseInt(team_order[newTurnIdx]));

	var tmpJSON = JSON.parse(JSON.stringify(gameJSON));
	tmpJSON.turn = parseInt(team_order[newTurnIdx]);
	
	var tmpLog = {
		user_id: 0,
		description: `Change turn from team ${oldTurnLabel} to team ${newTurnLabel}`,
		json_data: JSON.stringify(tmpJSON)
	}

	game_master.emit("action", tmpLog, (err) => {
		if(err) error_notif(`Whoops! Something went wrong.`);
	});
}

/*					
 * Board Panel
 */

$('#detail_btn-start-timer').click(() => {
	var tmpJSON = JSON.parse(JSON.stringify(gameJSON))
	tmpJSON.timer.problem.end = Date.now() + gameJSON.timer.problem.left
	tmpJSON.timer.problem.left = -1
	
	var tmpLog = {
		user_id: 0,
		description: 'Start problem timer',
		json_data: JSON.stringify(tmpJSON)
	}

	game_master.emit('action', tmpLog, (err) => {
		if(err) error_notif('Whoops! Something went wrong')
	})
})

$('#detail_btn-pause-timer').click(() => {
	var tmpJSON = JSON.parse(JSON.stringify(gameJSON))
	tmpJSON.timer.problem.end = -1
	tmpJSON.timer.problem.left = gameJSON.timer.problem.end - Date.now()

	var tmpLog = {
		user_id: 0,
		description: 'Pause problem timer',
		json_data: JSON.stringify(tmpJSON)
	}

	game_master.emit('action', tmpLog, (err) => {
		if(err) error_notif('Whoops! Something went wrong')
	})
})

$('#detail_btn-reset-timer').click(() => {
	var tmpJSON = JSON.parse(JSON.stringify(gameJSON))
	tmpJSON.timer.problem.end = -1
	tmpJSON.timer.problem.left = gameProblem.time_limit

	var tmpLog = {
		user_id: 0,
		description: 'Reset problem timer',
		json_data: JSON.stringify(tmpJSON)
	}

	game_master.emit('action', tmpLog, (err) => {
		if(err) error_notif('Whoops! Something went wrong')
	})
})

$('#detail_prev-slide').click(() => {
	changeBoxSlide(active_box, -1, 'Next slide')
})
$('#detail_next-slide').click(() => {
	changeBoxSlide(active_box, +1, 'Next slide')
})
$('#detail_btn-show-desc').click(() => {
	changeBoxSlide(active_box, +1, 'Show problem description')
})
$('#detail_btn-hide-desc').click(() => {
	changeBoxSlide(active_box, -1, 'Hide problem description')
})

$('#detail_btn-correct-ans').click(() => {
	game_master.emit('verdict', 'correct', (err) => {
		if(err) error_notif('Whoops! Something went wrong')
		else{
			var tmpJSON = JSON.parse(JSON.stringify(gameJSON))
			tmpJSON.box.slide[active_box] = -1
			tmpJSON.box.status[active_box] = gameJSON.turn
			tmpJSON.team[gameJSON.turn].score += gameSettings['score_correct']

			if(gameProblem.type === 0){
				tmpJSON.team[gameJSON.turn].last_boxType = 0
			}else if(gameProblem.type <= 3){
				tmpJSON.team[gameJSON.turn].last_boxType = gameProblem.type
			}

			var teamChar = String.fromCharCode(64 + gameJSON.turn)
			var tmpLog = {
				user_id: 0,
				description: `Team ${teamChar} successfully answer box ${active_box}`,
				json_data: JSON.stringify(tmpJSON)
			}

			game_master.emit('action', tmpLog, (err) => {
				if(err) error_notif('Whoops! Something went wrong')
			})
		}	
	})
})
$('#detail_btn-wrong-ans').click(() => {
	game_master.emit('verdict', 'wrong', (err) => {
		if(err) error_notif('Whoops! Something went wrong')
		else{
			var tmpJSON = JSON.parse(JSON.stringify(gameJSON))
			tmpJSON.box.slide[active_box] = -1
			tmpJSON.team[gameJSON.turn].score += gameSettings['score_wrong']

			if(gameProblem.type === 0){
				tmpJSON.team[gameJSON.turn].last_boxType = 0
			}else if(gameProblem.type > 3){
				tmpJSON.team[gameJSON.turn].last_boxType = gameProblem.type
			}

			var teamChar = String.fromCharCode(64 + gameJSON.turn)
			var tmpLog = {
				user_id: 0,
				description: `Team ${teamChar} failed to answer box ${active_box}`,
				json_data: JSON.stringify(tmpJSON)
			}

			game_master.emit('action', tmpLog, (err) => {
				if(err) error_notif('Whoops! Something went wrong')
			})
		}	
	})
})

function changeBoxSlide(boxNum, val, msg){
	if(gameJSON == null) return

	var tmpJSON = JSON.parse(JSON.stringify(gameJSON))
	tmpJSON.box.slide[boxNum] = parseInt(tmpJSON.box.slide[boxNum]) + parseInt(val)
	
	var tmpLog = {
		user_id: 0,
		description: msg,
		json_data: JSON.stringify(tmpJSON)
	}

	game_master.emit('action', tmpLog, (err) => {
		if(err) error_notif('Whoops! Something went wrong')
	})
}

function startProblemCountdown(timeEnd){
	$('#detail_btn-start-timer').css('display', 'none')
	$('#detail_btn-pause-timer').css('display', 'inline-block')

	$("#detail_timer-label").countdown(timeEnd);
}

function stopProblemCountdown(timeLeft){
	$('#detail_btn-start-timer').css('display', 'inline-block')
	$('#detail_btn-pause-timer').css('display', 'none')

	$("#detail_timer-label").countdown('stop');
	
	if(timeLeft == 0){
		$('#detail_btn-start-timer').attr("disabled", "true");
	}else{
		$('#detail_btn-start-timer').removeAttr("disabled");
	}

	var ss = Math.floor(timeLeft / 1000)
	var ms = timeLeft % 1000
	
	if(ss < 10) ss = '0' + ss.toString()
	if(ms < 10) ms = '00' + ms.toString()
	else if(ms < 99) ms = '0' + ms.toString()

	$("#detail_timer-label").html(`${ss}.${ms} s`)
}

function showDetailControl(num){
	if(gameJSON.box.status[num] !== 0){
		error_notif(`Sorry, you can't open non-empty box`)
		return
	}

	var tmpJSON = JSON.parse(JSON.stringify(gameJSON));
	tmpJSON.box.slide[num] = 0;

	var tmpLog = {
		user_id: 0,
		description: `Show box ${num} control`,
		json_data: JSON.stringify(tmpJSON)
	}

	game_master.emit("action", tmpLog, (err) => {
		if(err) error_notif(`Failed to open box ${num} control`);
	});
}

function closeDetailControl(){
	var tmpJSON = JSON.parse(JSON.stringify(gameJSON));
	tmpJSON.box.slide = tmpJSON.box.slide.map(v => -1);
	
	var tmpLog = {
		user_id: 0,
		description: `Show box ${active_box} control`,
		json_data: JSON.stringify(tmpJSON)
	}

	game_master.emit("action", tmpLog, (err) => {
		if(err) error_notif(`Failed to close box ${active_box} control`);
	});
}

/*					
 * Scoreboard Panel
 */
function updateScoreboard(teamNew, teamOld){
	for(let i = 1; i < teamNew.length; i++){
		if(teamOld){
			var scoreNew = parseInt(teamNew[i].score),
				scoreOld = parseInt(teamOld[i].score);
			
			if(scoreNew != scoreOld){
				$(`#${i}.team-box .team-box-tool`).shuffleLetters({
					text: String(scoreNew)
				});
			}
		}else{
			$(`#${i}.team-box .team-box-tool`).shuffleLetters({
				text: String(teamNew[i].score)
			});
		}
	}
}

/*					
* Score Panel
*/

$('#input_score-new').keypress(function(event){
	var keycode = (event.keyCode ? event.keyCode : event.which);
	if(keycode == '13'){
		saveInputScore(); 
	}
});

// Get old team score
$("#input_score-team").on("change", () => {
	var idx = $("#input_score-team").val();
	$("#input_score-old").val(gameJSON.team[idx].score);
});

function resetInputScore(){
	$("#input_score-team").val("0");
	$("#input_score-old").val("");
	$("#input_score-new").val("");
}

function saveInputScore(){
	var idx = parseInt($("#input_score-team").val());
	var input_score = $("#input_score-new").val();
	var oldScore = parseInt($("#input_score-old").val()),
		newScore;
	var teamChar = String.fromCharCode(65 + idx - 1);
	var act_desc = "";

	if(idx == 0) return;

	// Parsing score input
	// If score input is -x or +x then add old score with -x or +x.
	// But, if score input is x then change old score to x
	if(input_score[0] == '-' || input_score[0] == '+'){
		newScore = oldScore + parseInt(input_score);
		
		if(input_score[0] == '-'){
			act_desc = `Subtracting ${input_score} points on team ${teamChar} score`;
		}else{
			act_desc = `Adding ${input_score} points on team ${teamChar} score`;
		}
	}else{
		newScore = parseInt(input_score);
		act_desc = `Change team ${teamChar} score from ${oldScore} to ${newScore} points`;
	}

	// Create new Gamelog json_data
	var tmpGameJSON = JSON.parse(JSON.stringify(gameJSON));
	tmpGameJSON.team[idx].score = newScore;
	
	var tmpLog = {
		user_id: 0,
		description: act_desc,
		json_data: JSON.stringify(tmpGameJSON)
	}

	// Request update gamelog
	game_master.emit("action", tmpLog, (err) => {
		if(err) error_notif(`Whoops! Something went wrong.`);

		resetInputScore();
	});
}

/*					
 * Box Panel
 */

$("#input_box-number").on("change", function(){
	var box_number = parseInt($(this).val());
	var box_owner = gameJSON.box.status[box_number];
	var name_owner = "";
	
	if(box_owner == 0) name_owner = "None";
	else name_owner = `${String.fromCharCode(64 + box_owner)} - ${$(`#team_name-${box_owner}`).val()}`;

	$("#input_box-from").val(box_owner);
	$("#input_box-from-label").val(name_owner);
});

function resetInputBox(){
	$("#input_box-number").val("0");
	$("#input_box-from").val("");
	$("#input_box-from-label").val("");
	$("#input_box-to").val("0");
}

function saveInputBox(){
	var box_number = parseInt($("#input_box-number").val());
	var box_from = parseInt($("#input_box-from").val());
	var box_to = parseInt($("#input_box-to").val());

	var team_from, team_to;
	
	if(box_from == 0) team_from = "None";
	else team_from = `team ${String.fromCharCode(64 + box_from)}`;

	if(box_to == 0) team_to = "None";
	else team_to = `team ${String.fromCharCode(64 + box_to)}`;

	if(box_number == 0) return;

	var tmpGameJSON = JSON.parse(JSON.stringify(gameJSON));
	tmpGameJSON.box.status[box_number] = box_to;

	var tmpLog = {
		user_id: 0,
		description: `Moving the ownership of box ${box_number} from ${team_from} to ${team_to}`,
		json_data: JSON.stringify(tmpGameJSON)
	}

	game_master.emit("action", tmpLog, (err) => {
		if(err) error_notif(`Whoops! Something went wrong.`);

		resetInputBox();
	});
}