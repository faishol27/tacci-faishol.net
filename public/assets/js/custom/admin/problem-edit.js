/* Tinymce configuration */
tinymce.init({
	selector: "input[type=textarea]",
	toolbar: [
		"undo redo | styleselect | bold underline italic | alignleft aligncenter alignright alignjustify | bullist numlist",
		"table link image | preview"
	],
	plugins: "image imagetools link lists preview table",
	menubar: false,
	branding: false
});

/* Admin Page > Problem Edit page */
function submitProblem(){
	var prob_id = $("#problem_id").val();
	var prob_data = {};
	
	tinymce.triggerSave();
	
	prob_data.type = $("#type").val();
	prob_data.category_id = 0;
	prob_data.time_limit = $("#time_limit").val() * 1000;
	prob_data.description = $("#question").val();
	prob_data.answer = $("#answer").val();
	
	$.ajax({
		url: '/api/v1/problem/' + prob_id,
		method: 'PUT',
		data: prob_data,
		complete: function(res){
			if(res.status == 200 || res.status == 4){
				$.notify({
					icon: 'fa fa-check',
					message: 'Problem #' + prob_id + ' successfully updated'
				}, {
					placement: {
						from: "top",
						align: "center"
					},
					type: "success"
				});
			}else{
				$.notify({
					icon: 'fa fa-ban',
					message: 'Problem #' + prob_id + ' failed to updated'
				}, {
					placement: {
						from: "top",
						align: "center"
					},
					type: "danger"
				});
			}
		}
	});
}
