$(".sortable").sortable();

// Prevent edit problem order when the game already started
var game_client = io.connect('/game-client');
game_client.on("signal", (data) => {
    if(data === "start"){
		$("#btnSaveorder").attr("disabled", "true");
        $("#btnShuffle").attr("disabled", "true");
		$("#btnReorder").attr("disabled", "true");
		
		$.notify({
			icon: 'fa fa-info',
			message: `&nbsp;&nbsp;Sorry, you can't edit problem order when the game already started.`
		}, {
			placement: {
				from: "top",
				align: "center"
			},
			type: "info"
		});
    }else{
		$("#btnSaveorder").removeAttr("disabled");
        $("#btnShuffle").removeAttr("disabled");
		$("#btnReorder").removeAttr("disabled");
	}
});

/* Admin Page > Problems Page > Problem Card */
function resetProblemNumbering(probs){
    for(let i = 0; i < probs.length; i++){
		var boxNumber = probs[i].getElementsByClassName('box-number')[0];
		boxNumber.innerHTML = "BOX "+parseInt(i+1);
	}
}

/* Random Shuffle */
function randomOrderProblem(){
	// Get random order array
	var tmp_arr = $("#problem_order").val().split(","),
	tmp_len = tmp_arr.length;
	var rand_order = [];
	
	for(let i = 0; i < parseInt(tmp_len); i++){
		let idx = Math.floor(Math.random() * parseInt(tmp_len - i));
		rand_order.push(tmp_arr[idx]);
		tmp_arr.splice(idx, 1);	
	}
	
	var prob_card = $(".prob-card");
	for(let i = 0; i < rand_order.length; i++){
		$(".prob-card#" + rand_order[i]).insertAfter(prob_card.last());
		prob_card = $(".prob-card");
	}
	
	$("#problem_order").val(rand_order.join(','));
	resetProblemNumbering(prob_card);
	
	$.notify({
		icon: 'fa fa-check',
		message: 'Problem order successfully randomized'
	}, {
		placement: {
			from: "top",
			align: "center"
		},
		type: "success"
	});
}

function confirmRandomShuffle(){
	bootbox.confirm({ 
		size: "small",
		title: "Confirmation",
		message: "Are you sure you want to randomized problem order?",
		buttons: {
			confirm: {
				label: 'Yes',
				className: 'btn btn-warning'
			},
			cancel: {
				label: 'No',
				className: 'btn btn-default'
			}
		},
		callback: function(result){
			if(result == true) randomOrderProblem();
		}
	});
};

function toggleOrderProblem(){
	var isDisabled = $("#problem_interact").sortable( "option", "disabled" );
	
    if(isDisabled){
		$(".prob-card-title .order-icon").css("opacity", 0);
		
		$("#problem_interact").sortable("option", "disabled", false);
		
		$("#btnSaveorder").css("display", "inline-block");
        $("#btnShuffle").css("display", "inline-block");
        $("#btnReorder").css("display", "none");
    }else{
		$(".prob-card-title .order-icon").css("opacity", 1);
		
		$("#problem_interact").sortable("option", "disabled", true);
		
		$("#btnSaveorder").css("display", "none");
        $("#btnShuffle").css("display", "none");
		$("#btnReorder").css("display", "inline-block");
		
		// Send update setting "problem_order" to database
		$.ajax({
			url: "/api/v1/setting/problem_order",
			method: "PUT",
			data: {"value": $("#problem_order").val()},
			complete: function(res){
				if(res.status == 200 || res.status == 4){
					$.notify({
						icon: 'fa fa-check',
						message: 'Problem order successfully updated'
					}, {
						placement: {
							from: "top",
							align: "center"
						},
						type: "success"
					});
				}else{
					$.notify({
						icon: 'fa fa-ban',
						message: 'Problem order failed to updated'
					}, {
						placement: {
							from: "top",
							align: "center"
						},
						type: "danger"
					});
				}
			}
		});
    }
}

var probs = document.getElementsByClassName("prob-card-title");
resetProblemNumbering(probs);

for(let i = 0; i < probs.length; i++){
	probs[i].addEventListener("click", function() {
		/* Toggle between hiding and showing the active panel */
        var probTitleDesc = this.getElementsByClassName('spoiler-desc');
        var probBody = this.nextElementSibling;

    	if(probBody.style.height){
			probBody.style.height = null;
            probTitleDesc[0].style.opacity = 1;
    	}else{
			probBody.style.height = probBody.scrollHeight + "px";
            probTitleDesc[0].style.opacity = 0;
    	}
  	});
}

$("#problem_interact").sortable({
    update: function(event, ui){
        var sortedIDs = $("#problem_interact").sortable("toArray");
		$("#problem_order").val(sortedIDs.join(','));
        resetProblemNumbering(probs);
    },
    disabled: true
});