/* Tooltip configuration */
$('[data-toggle="tooltip"]').tooltip();

/* Filepond Configuration */
FilePond.registerPlugin(FilePondPluginFileValidateType);
FilePond.setOptions({ server: '/process/filepond' });

const inputElement = document.querySelector('input[type="file"]');
const pond = FilePond.create(inputElement, {
	acceptedFileTypes: ['image/*']
});

function warningNotify(msg){
  $.notify({
    icon: 'fa fa-exclamation-triangle', message: msg
  },{ 
    placement: { from: "top", align: "center" }, type: "warning"
  })
}

function dangerNotify(msg){
	$.notify({
		icon: 'fa fa-ban', message: msg 
	},{
		placement: { from: "top", align: "center" }, type: "danger"
	})
}

/* Admin Page > Repositories > Upload */
function submitUpload(){
	var inputFileID = $('input[type=hidden]')
  var uploadFileID = []
  
  for(let i = 0; i < inputFileID.length; i++) uploadFileID.push(inputFileID[i].value)

  var allUploaded = pond.getFiles().every(v => v.status === 5)
  var fileError = pond.getFiles().every(v => v.status === 6 || v.status === 8 || v.status === 10)
  
  if (fileError === true) {
    dangerNotify('Remove all the error files first before uploading to the server')
  } else if (allUploaded === false) {
    warningNotify('Files are being uploaded')
  } else if (uploadFileID.length > 0) {
    $.ajax({
      url: '/process/uploads',
      method: 'POST',
      data: { 'filenames': uploadFileID.join(';') },
			success: function(){
				location.reload()
			},
      error: function (xhr, ajaxOptions, thrownError) {
        warningNotify('Whoops! Failed to upload files')
			}
    })
  }
}

/* Admin Page > Repository Page > Delete Button */
$('.delete-btn').click((event) => {
	var id = $(event.target).attr('id').slice(5)
	var filename = $(`#filename-${id}`).html()
  
	bootbox.confirm({
	  size: 'medium',
	  title: 'confirmation',
	  message: `Are you sure you want to delete '${filename}'?`,
	  buttons: {
		  confirm: {
		    label: 'Yes',
		    className: 'btn btn-danger'
		  },
		  cancel: {
		    label: 'No',
		    className: 'btn btn-default'
		  }
	  },
	  callback: (result) => {
		  if (result === true) {
		    $.ajax({
		  	  url: `/api/v1/upload/${id}`,
          method: 'DELETE',
          success: function(){
						location.reload()
					},
		  	  error: function(){
						warningNotify('Whoops! Failed to delete the files')
					}
		    })
		  }
	  }
	})
})

/* Admin Page > Repository Page > Copy Button */
$('.copy-btn').click((event) => {
	var elm = $(event.target)
	var id = elm.attr('id').slice(5)
	
	elm.attr('data-original-title', 'Link Copied!').tooltip('show');

	var dummyInput = $('<input>');
  $('body').append(dummyInput);
	
	dummyInput.val($(`#link-${id}`).text()).select();
  document.execCommand('copy');
	
	dummyInput.remove();
})

$('.copy-btn').mouseleave((event) => {
	var elm = $(event.target)
	elm.attr('data-original-title', 'Copy Link')
})