$(".sortable").sortable();
$('#box_fontcolor').colorpicker({
	inline: true,
	container: true,
	format: null,
	customClass: 'colorpicker-2x',
	sliders: {
	  saturation: {
		maxLeft: 200,
		maxTop: 200
	  },
	  hue: {
		maxTop: 200
	  },
	  alpha: {
		maxTop: 200
	  }
	}
}).on('colorpickerChange colorpickerCreate', function (e) {
	var colors = e.color.toRgbString();
	$('#box_fontcolor').attr('data-color', colors)
});

// Prevent edit setting when the game already started
var game_client = io.connect('/game-client');
game_client.on("signal", (data) => {
	if(data == "start"){
		if($('.overlay').length == 0){
			$(".overlay-wrapper").append("<div class='overlay' id='game_active'></div>");
		}
		
		$.notify({
			icon: 'fa fa-info',
			message: `&nbsp;&nbsp;Sorry, you can't edit setting when the game already started.`
		}, {
			placement: {
				from: "top",
				align: "center"
			},
			type: "info"
		});
	}else{
		if($('.overlay').length > 0){
			$(".overlay-wrapper #game_active").remove();
		}
	}
});

/* Admin Page > Settings Page */
function submitSettings(){
	var data = {};
	
	// Fetch data from General panel
	var inp_general = $("#general-panel input[type=text]");
	for(let i = 0; i < inp_general.length; i++){
		let db_id = inp_general[i].id.slice(8); 
		data[db_id] = inp_general[i].value;
	}

	// Fetch data from Game panel
	data["time_limit"] = parseInt($('#hours').val()) * 3600 + parseInt($('#minutes').val()) * 60
						+ parseInt($('#seconds').val());
	data["time_limit"] = data["time_limit"]*1000;
	
	// Fetch data from Scoring panel
	var inp_scoring = $("#scoring-panel input[type=number]");
	for(let i = 0; i < inp_scoring.length; i++){
		data[inp_scoring[i].id] = inp_scoring[i].value;
	}
	
	data['box_fontcolor'] = $('#box_fontcolor').attr('data-color');

	// Fetch data from Layout panel
	var inp_layout = $("#layout-panel input[type=number]");
	for(let i = 0; i < inp_layout.length; i++){
		data[inp_layout[i].id] = inp_layout[i].value;
	}

	// Fetch data from Team panel
	data["team_number"] = $("#team-panel #team_number").val();
	data["team_order"] = $("#team-panel #team_order").val();
	
	// Sent settings update to database
	$.ajax({
		url: '/api/v1/settings',
		method: 'PUT',
		data: data,
		complete: function(res){
			if(res.status == 200 || res.status == 4){
				$.notify({
					icon: 'fa fa-check',
					message: 'Settings successfully updated'
				}, {
					placement: {
						from: "top",
						align: "center"
					},
					type: "success"
				});
			}else{
				$.notify({
					icon: 'fa fa-ban',
					message: 'Settings failed to updated'
				}, {
					placement: {
						from: "top",
						align: "center"
					},
					type: "danger"
				});
			}
		}
	});

	// Updating team to database
	for(let i = 1; i <= team_number; i++){
		closeEditTeam(i);
	}
}

/* Admin Page > Settings Page > Team */
function getDBTeamId(idx){
    console.log(idx)
    $.get(`/api/v1/teams`, { "from": idx, "to": idx }, (data, status) => {
        $(`#${idx}.team-box #team_id-${idx}`).val(data.response[0].id);
    });
}

var team_number = parseInt($("#team_number").val());
var bg = ["bg-blue", "bg-red", "bg-yellow", "bg-green", "bg-purple", "bg-orange"];

$(document).ready(function(){
    for(let i = 1; i <= team_number; i++){
        $(`#${i}.team-box .team-box-icon`).addClass(bg[(i-1) % bg.length]);
    }
});

$("#team_number").on("change", function(){
    var new_teamNumber = parseInt($(this).val());
    if(team_number > new_teamNumber){
        for(let i = new_teamNumber + 1; i <= team_number; i++){
            $(`#${i}.team-box`).remove();
        }
    }else{
        for(let i = team_number + 1; i <= new_teamNumber; i++){
            $("#team_interact").append(
                `<div class="team-box" id="${i}">
                    <div class="team-box-icon ${bg[(i-1) % bg.length]}">
                        ${String.fromCharCode(65 + i - 1)}
                    </div>
			    	<div class="team-box-content">
			    		<a id="edit_icon" href="javascript: editTeam(${i});" class="team-box-tool"><i class="fa fa-edit"></i></a>
			    		<a id="close_icon" href="javascript: closeEditTeam(${i});" class="team-box-tool" hidden><i class="fa fa-times"></i></a>
			    		<input id="team_id-${i}" hidden>
			    		<input value="Nama Tim" id="team_name-${i}" class="team-box-title" type="text" readonly>
			    		<input value="Asal Sekolah" id="team_school-${i}" class="team-box-subtitle" type="text" readonly>
			    	</div>							
                </div>`
            );

            getDBTeamId(i);
        }
    }
    
    // Updating #team_order
    $("#team_interact").sortable('refresh');
    var sortedIDs = $("#team_interact").sortable("toArray");
    $("#team_order").val(sortedIDs.join(','));
    
    team_number = new_teamNumber;
});

$("#team_interact").sortable({
	update: function(event, ui){
		var sortedIDs = $("#team_interact").sortable("toArray");
		$("#team_order").val(sortedIDs.join(','));
	}
});

function editTeam(i){
	$("#team_interact #" + i + " #team_name").prop("readonly", false);
	$("#team_interact #" + i + " #team_school").prop("readonly", false);
	$("#team_interact #" + i + " #edit_icon").prop("hidden", true);
	$("#team_interact #" + i + " #close_icon").prop("hidden", false);
}

function closeEditTeam(i){
	$("#team_interact #" + i + " #team_name").prop("readonly", true);
	$("#team_interact #" + i + " #team_school").prop("readonly", true);
	$("#team_interact #" + i + " #edit_icon").prop("hidden", false);
	$("#team_interact #" + i + " #close_icon").prop("hidden", true);
	
    // Updating data to database
    var team_id = $(`#team_id-${i}`).val();
	var team_name = $(`#team_name-${i}`).val();
	var team_school = $(`#team_school-${i}`).val();
    
    $.ajax({
        url: "/api/v1/team/" + parseInt(team_id),
        type: "PUT",
        data:  {"name": team_name, "school": team_school},
        complete: function(res){
			if(res.status == 200 || res.status == 4){
				$.notify({
					icon: 'fa fa-check',
					message: `Team ${parseInt(i)} successfully updated`
				}, {
					placement: {
						from: "top",
						align: "center"
					},
					type: "success"
				});
			}else{
				$.notify({
					icon: 'fa fa-ban',
					message: `Team ${parseInt(i)} failed to updated`
				}, {
					placement: {
						from: "top",
						align: "center"
					},
					type: "danger"
				});
			}
		}
    });
}