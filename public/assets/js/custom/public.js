$(document).ready(() => {
	$('.side').css('height', '175px')
	$('.side').css('width', '175px')
	$('.side').css('min-width', '100px')
})

var gameClient = io.connect('/game-client')
var gameJSON
var gameProblem
var activeBox

var bgBoard = ["bg-blue", "bg-red", "bg-yellow", "bg-green", "bg-purple", "bg-orange"]
var	lenBgBoard = bgBoard.length

$('.shape').shape({silent: true})

$("#problem_timer-label")
	.countdown(Date.now() + 2, {precision: 0, showMiliseconds: true})
	.on("update.countdown", function(event) {
		$(this).text( event.strftime('%S s') )
	})
	.countdown('stop')

$("#main_timer-label")
	.countdown(Date.now() + 2, {precision: 10})
	.on("update.countdown", function(event) {
		$(this).text( event.strftime('%M : %S') )
	})
	.on("finish.countdown", function(event){
		$(this).text( event.strftime('%M : %S') )
	})
	.countdown('stop')

async function gameController(oldJSON, newJSON){
	mainTimerController(newJSON.timer.main)
	problemTimerController(newJSON.timer.problem)
	
	if(oldJSON == null){
		if(newJSON.problem_id !== 0){
			await $.ajax({
				url: `/api/v1/problem/${newJSON.problem_id}`,
				method: 'GET',
				success: function(data){
					gameProblem = data.response[0]
				}
			})
		}
	
		ownerController(null, newJSON.box.status)
		gameTurnController(null, newJSON.turn)
		scoreboardController(null, newJSON.team)
		boxAnimationController(null, newJSON.box.slide)
	}else{
		if(newJSON.problem_id !== oldJSON.problem_id && newJSON.problem_id !== 0){
			await $.ajax({
				url: `/api/v1/problem/${newJSON.problem_id}`,
				method: 'GET',
				success: function(data){
					gameProblem = data.response[0]
				}
			})
		}else if(newJSON.problem_id === 0) gameProblem = null

		ownerController(oldJSON.box.status, newJSON.box.status)
		gameTurnController(oldJSON.turn, newJSON.turn)
		scoreboardController(oldJSON.team, newJSON.team)
		boxAnimationController(oldJSON.box.slide, newJSON.box.slide)
	}
}

gameClient.on("command", (data) => {
	gameController(gameJSON, JSON.parse(data.json_data))
	gameJSON = JSON.parse(data.json_data)
})

gameClient.on("signal", (data) => {
	if(data === 'not started'){
		$('#loading-board').removeClass('deactive')	
	}else{
		$('#loading-board').addClass('deactive')
	}
})

gameClient.on("verdict", (data) => {
	if(data === 'correct'){
		$('#verdict-icon').attr('src', '/img/grin-beam.svg')
		$('#verdict-label').html('<strong>CORRECT</strong>')
	}else{
		$('#verdict-icon').attr('src', '/img/dizzy.svg')
		$('#verdict-label').html('<strong>WRONG ANSWER</strong>')
	}

	$('#verdict-board').removeClass('deactive')

	setInterval(() => {
		$('#verdict-board').addClass('deactive')
	}, 2000)
})

function mainTimerController(timer) {
	if(timer.left >= 0){
		var timeLeft = timer.left

		$("#label_main-timer").countdown('stop')

		var secLeft = Math.floor(timeLeft / 1000)
		var mm = Math.floor(secLeft / 60) % 60
		var ss = secLeft % 60

		if(mm < 10) mm = "0" + mm.toString();
		if(ss < 10) ss = "0" + ss.toString();

		$("#main_timer-label").html(`${mm} : ${ss}`);
	
	}else{
		var timeEnd = timer.end
		$('#main_timer-label').countdown(timeEnd)	
	}
}

function problemTimerController(timer) {
	if(timer.end !== -1){
		var timeEnd = timer.end
		$('#problem_timer-label').countdown(timeEnd)

	}else if(timer.left !== -1){
		var timeLeft = timer.left
		$("#problem_timer-label").countdown('stop')

		var ss = Math.floor(timeLeft / 1000)
		var ms = timeLeft % 1000
		
		if(ss < 10) ss = '0' + ss.toString()
		if(ms < 10) ms = '00' + ms.toString()
		else if(ms < 99) ms = '0' + ms.toString()

		$("#problem_timer-label").html(`${ss}.${ms} s`)
	}
}

function scoreboardController(teamOld, teamNew){
	teamNew.forEach((v, idx) => {
		if(idx == 0) return

		if(teamOld){
			var scoreNew = parseInt(v.score)
			var scoreOld = parseInt(teamOld[idx].score)
			
			if(scoreNew !== scoreOld){
				$(`#${idx}.team-box .team-box-tool`).shuffleLetters({
					text: String(scoreNew)
				});
			}
		}else{
			$(`#${idx}.team-box .team-box-tool`).shuffleLetters({
				text: String(v.score)
			});
		}
	})
}

function gameTurnController(turnOld, turnNew){
	if(turnOld){
		$(`.team-box#${turnOld} .team-box-icon`).css('opacity', '0.25')
		$(`.team-box#${turnOld} .team-box-content`).css('color', '#d3d3d3')
	}

	$(`.team-box#${turnNew} .team-box-icon`).css('opacity', '1')
	$(`.team-box#${turnNew} .team-box-content`).css('color', '#000')
}

async function boxAnimationController(slideOld, slideNew){
	slideNew.forEach((v, i) => {
		if(slideOld){
			var sideNew = Math.max(v, 0)
			var sideOld = Math.max(slideOld[i], 0)
			
			if(sideNew <= 1){
				$(`#box-${i}`).shape('set next side', `#box-${i} #side-${v}`)

				if(sideNew < sideOld){
					$(`#box-${i}`).shape('flip left')
				}else if(sideNew > sideOld){
					$(`#box-${i}`).shape('flip right')
				}
			}
			
		}else{
			if(v !== -1 && v <= 1){
				$(`#box-${i}`)
					.shape('set next side', `#box-${i} #side-${v}`)
					.shape('flip right')
			}
		}
	})
	
	var activeProbBoard = slideNew.findIndex(v => v >= 2)
	if(activeProbBoard !== -1){
		$('#problem-board').removeClass('deactive')

		if(slideNew[activeProbBoard] === 3){
			$('#problem-body').html(gameProblem.description)
		}else{
			$('#problem-body').html('')
		}
	}else{
		$('#problem-board').addClass('deactive')
	}
}

function ownerController(sttOld, sttNew){
	if(sttOld){
		sttOld.forEach((v, idx) => {
			if(v !== sttNew[idx]){
				if(v === 0) $(`#box-${idx} .side`).removeClass('bg-default')
				else if(v !== 0) $(`#box-${idx} .side`).removeClass(bgBoard[(v - 1) % lenBgBoard])
				
				var newOwner = sttNew[idx]
				
				if(newOwner === 0){
					$(`#box-${idx} .side`).addClass('bg-default')
					$(`#box-${idx} .side h1`).html(idx)
				}else{
					$(`#box-${idx} .side`).addClass(bgBoard[(newOwner - 1) % lenBgBoard])
					$(`#box-${idx} .side h1`).html(String.fromCharCode(64 + newOwner))
				}
			}
		})
	}else{
		sttNew.forEach((v, idx) => {
			if(v !== 0){
				$(`#box-${idx} .side`).addClass(bgBoard[(v - 1) % lenBgBoard])
				$(`#box-${idx} .side h1`).html(String.fromCharCode(64 + v))
			}else{
				$(`#box-${idx} .side`).addClass('bg-default')
			}
		})
	}
}