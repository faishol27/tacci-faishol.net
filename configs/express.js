require('dotenv').config()

// NPM Modules
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const express = require('express')
const session = require('express-session')
const path = require('path')

// Configs
const passport = require(path.join(process.env.APP_PATH, 'configs/passport'))

// Variables
let sessionDetails = {
  secret: 'i6l8jdkhDfsijM0cWra7dczkDrcNUtLK',
  resave: false
}

// Configure Express
const app = express()
app.set('view engine', 'ejs')

app.use(express.static(path.join(process.env.APP_PATH, 'public/')))
app.use(express.static(path.join(process.env.APP_PATH, 'public/assets')))
app.use(express.static(path.join(process.env.APP_PATH, 'node_modules')))

app.use(cookieParser())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.text())

app.use(session(sessionDetails))

app.use(passport.initialize())
app.use(passport.session())

module.exports = app
