require('dotenv').config()

// NPM Modules
const Sequelize = require('sequelize')

// Configure Sequelize
module.exports = new Sequelize({
  host: (process.env.DB_HOST || 'localhost'),
  username: (process.env.DB_USER || 'root'),
  password: (process.env.DB_PASS || null),
  database: (process.env.DB_NAME || 'tacci'),
  dialect: (process.env.DB_DRIVER || 'mysql'),
  define: {
    timestamps: false
  }
})
