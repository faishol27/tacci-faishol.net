require('dotenv').config()

// NPM Modules
const fs = require('fs')
const path = require('path')
const { createLogger, transports, format } = require('winston')

// Variables
let APP_PATH = process.env.APP_PATH

const dateToday = new Date()
const today = `${dateToday.getFullYear()}-${dateToday.getMonth()}-${dateToday.getUTCDay()}`

const logFolder = path.join(APP_PATH, `/logs/${today}`)
const logFileErr = path.join(logFolder, 'error.log')
const logFileWarn = path.join(logFolder, 'warning.log')
const logFileCombine = path.join(logFolder, 'combined.log')

if (!fs.existsSync(path.join(APP_PATH, '/logs'))) {
  fs.mkdirSync(path.join(APP_PATH, '/logs'))
}

if (!fs.existsSync(logFolder)) {
  fs.mkdirSync(logFolder)
}

const logger = createLogger({
  format: format.combine(
    format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
    format.json()
  ),
  transports: [
    new transports.File({ filename: logFileErr, level: 'error' }),
    new transports.File({ filename: logFileWarn, level: 'warn' }),
    new transports.File({ filename: logFileCombine })
  ]
})

if (process.env.NODE_ENV !== 'production') {
  logger.add(new transports.Console())
}

module.exports = logger
