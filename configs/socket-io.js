require('dotenv').config()

// NPM Modules
const http = require('http')
const path = require('path')
const Socketio = require('socket.io')

// Variables
const APP_PATH = process.env.APP_PATH

// Configs
const app = require(path.join(APP_PATH, '/configs/express'))
const server = http.Server(app)
const io = new Socketio(server)

// Models
const GamelogMdl = require(path.join(APP_PATH, '/models/gamelog'))
const ProblemMdl = require(path.join(APP_PATH, '/models/problem'))
const SettingMdl = require(path.join(APP_PATH, '/models/setting'))
const TeamMdl = require(path.join(APP_PATH, '/models/team'))

// Global function
function getSettings () {
  return new Promise((resolve, reject) => {
    SettingMdl.findAll().then((rows) => {
      var ret = {}

      rows.forEach(v => {
        if (v.data_type === 'int') {
          ret[v.name] = parseInt(v.value)
        } else {
          ret[v.name] = String(v.value)
        }
      })
      resolve(ret)
    }).catch(err => {
      reject(err)
    })
  })
}

function initGamelogJSON () {
  return new Promise(async (resolve, reject) => {
    try {
      var settings = await getSettings().catch(err => { throw err })
      var teams = await TeamMdl
        .findAll({ limit: settings['team_number'], order: [['id', 'ASC']] })
        .then(rows => {
          var ret = []

          // Dummy data
          ret.push({})

          rows.forEach(v => {
            ret.push({
              team_id: v.id,
              score: 0,
              last_boxType: 0
            })
          })

          return ret
        })
        .catch(err => { throw err })

      var jsonRet = {
        box: { bonus_stt: [], slide: [], status: [] },
        problem_id: 0,
        team: teams,
        timer: {
          main: {
            end: -1,
            left: parseInt(settings['time_limit'])
          },
          problem: {
            end: -1,
            left: -1
          }
        },
        turn: parseInt(settings['team_order'].split(',')[0])
      }

      // Dummy for index 0
      jsonRet.box.status.push(0)
      jsonRet.box.slide.push(-1)
      jsonRet.box.bonus_stt.push([])

      // Inserting real data to jsonRet
      for (let i = 0; i < settings['board_row']; i++) {
        for (let j = 0; j < settings['board_column']; j++) {
          jsonRet.box.status.push(0)
          jsonRet.box.slide.push(-1)
          jsonRet.box.bonus_stt.push([])
        }
      }
    } catch (error) {
      reject(error)
      return
    } finally {
      resolve(jsonRet)
    }
  })
}

async function ticTacToeChecker (dataLog) {
  // This function will count number of group that contain 3-consecutive box with same owner
  // and give bonus points to the box owner
  return new Promise(async (resolve, reject) => {
    try {
      // Settings variables
      var settings = await getSettings().catch(err => { throw err })
      var row = settings['board_row']

      var col = settings['board_column']
      var bonusScore = settings['score_bonus']

      // Gamelog variables
      var boxStatus = dataLog.box.status
      var bonusStt = dataLog.box.bonus_stt

      // Pointer movement, number is step from middle point
      var mvCol = {
        left: [[-1, -2], [0, 0], [-1, -2], [-1, -2]],
        right: [[+1, +2], [0, 0], [+1, +2], [+1, +2]]
      }
      var mvRow = {
        left: [[0, 0], [-1, -2], [-1, -2], [+1, +2]],
        right: [[0, 0], [+1, +2], [+1, +2], [-1, -2]]
      }

      // Checking
      for (let i = 0; i < row; i++) {
        for (let j = 0; j < col; j++) {
          var centerNum = i * col + j + 1
          var owner = boxStatus[centerNum]

          if (owner === 0 || bonusStt[centerNum].indexOf(owner) !== -1) continue

          for (let k = 0; k < 4; k++) {
            var posCol = {
              left: mvCol.left[k].map(v => j + v),
              right: mvCol.right[k].map(v => j + v)
            }
            var posRow = {
              left: mvRow.left[k].map(v => i + v),
              right: mvRow.right[k].map(v => i + v)
            }
            var number = {
              left: posCol.left.map((pC, idx) => {
                var pR = posRow.left[idx]
                if (pC >= 0 && pC < col && pR >= 0 && pR < row) {
                  return pR * col + pC + 1
                }
              }),
              right: posCol.right.map((pC, idx) => {
                var pR = posRow.right[idx]
                if (pC >= 0 && pC < col && pR >= 0 && pR < row) {
                  return pR * col + pC + 1
                }
              })
            }
            var countGroup = {
              left: 0,
              right: 0
            }

            for (let i = 0; i < 2; i++) {
              if (number.left[i] && boxStatus[number.left[i]] === owner) {
                countGroup.left++
              } else break
            }

            for (let i = 0; i < 2; i++) {
              if (number.right[i] && boxStatus[number.right[i]] === owner) {
                countGroup.right++
              } else break
            }

            var totalGroup = Math.max(countGroup.left + countGroup.right - 1, 0)
            dataLog.team[owner].score += totalGroup * bonusScore
          }

          bonusStt[centerNum].push(owner)
        }
      }
    } catch (err) {
      return reject(err)
    } finally {
      resolve(dataLog)
    }
  })
}

function getProblemId (num) {
  return new Promise(async (resolve, reject) => {
    try {
      var settings = await getSettings().catch(err => { throw err })
      var probOrd = settings.problem_order.split(',')
      var prob = await ProblemMdl.findOne({ offset: parseInt(probOrd[num - 1]) - 1, limit: 1 })
        .then(row => {
          return row
        }).catch(err => { throw err })
    } catch (err) {
      return reject(err)
    } finally {
      resolve(prob)
    }
  })
}

// Socket.io namespace
var gameMaster = io.of('/game-master')
var gameClient = io.of('/game-client')

// Socket.io routing
gameMaster.on('connect', socket => {
  socket.on('switch', async (data, callback) => {
    if (data === 'start') {
      try {
        var settings = await getSettings().catch(err => { throw err })
        var gamelogJSON = await initGamelogJSON().catch(err => { throw err })
        var gamelogData = {
          user_id: 0,
          description: `Create a new game`,
          json_data: JSON.stringify(gamelogJSON)
        }

        // Destroy all old gamelog
        await GamelogMdl.destroy({ truncate: true }).catch(err => { throw err })

        // Initialize gamelog
        await GamelogMdl.create(gamelogData).catch(err => { throw err })

        // Update game_active value to 1
        await SettingMdl.update({ value: '1' }, { where: { name: 'game_active' } })
          .catch(err => { throw err })
      } catch (err) {
        console.log(err)
        return callback(new Error('error'))
      } finally {
        gameClient.emit('init', null, settings)
        gameClient.emit('signal', 'start')
        gameClient.emit('command', gamelogData)
      }

      return callback(null)
    } else if (data === 'stop') {
      try {
        // Update game_active value to 0
        await SettingMdl.update({ value: '0' }, { where: { name: 'game_active' } })
          .catch(err => { throw err })
      } catch (err) {
        console.log(err)
        return callback(new Error('error'))
      } finally {
        gameClient.emit('signal', 'stop')
      }

      return callback(null)
    } else {
      return callback(new Error('command not found'))
    }
  })

  socket.on('action', async (data, callback) => {
    try {
      var gameJSON = JSON.parse(data.json_data)
      var activeBox = gameJSON.box.slide.findIndex(v => v !== -1)
      var probActive

      gameJSON = await ticTacToeChecker(gameJSON).catch(err => { throw err })

      if (activeBox !== -1) {
        if (gameJSON.problem_id === 0) {
          probActive = await getProblemId(activeBox).catch(err => { throw err })

          gameJSON.problem_id = probActive.id
          gameJSON.timer.problem.left = probActive.time_limit          
          gameJSON.timer.problem.end = -1
        }
      } else {
        gameJSON.problem_id = 0

        gameJSON.timer.problem.left = -1
        gameJSON.timer.problem.end = -1
      }

      data.json_data = JSON.stringify(gameJSON)

      // Delete inactive log
      var inactive = {
        where: { active_log: 0 }
      }

      await GamelogMdl.destroy(inactive).catch(err => { throw err })

      // Validate there aren't duplicate
      var dataFilter = {
        where: {
          description: data.description,
          json_data: data.json_data
        }
      }

      var isDuplicate = await GamelogMdl.findAll(dataFilter).then(rows => {
        if(rows.length > 0) return true
      }).catch(err => { throw err })

      if(isDuplicate === true) return callback(null)

      // Insert new gamelog's data
      await GamelogMdl.create(data).catch(err => { throw err })

    } catch (err) {
      console.log(err)
      return callback(new Error('error'))

    } finally {
      gameClient.emit('command', data)
    
    }

    return callback(null)
  })

  socket.on('verdict', (data, callback) => {
    gameClient.emit('verdict', data)
    callback(null)
  })
})

gameClient.on('connect', async (socket) => {
  try {
    var settings = await getSettings().catch(err => { throw err })
    var gamelogData
    if (settings['game_active'] === 1) {
      gamelogData = await GamelogMdl.findOne({ limit: 1, order: [['id', 'DESC']] })
        .then(gameRows => {
          return gameRows
        })
        .catch(err => { throw err })
    }
  } catch (err) {
    console.log(err)
    return socket.emit('init', `error`)
  } finally {
    socket.emit('init', null, settings)

    if (settings['game_active'] === 1) {
      socket.emit('signal', 'start')
      socket.emit('command', gamelogData)
    } else {
      socket.emit('signal', 'not started')
    }
  }
})

module.exports = server
