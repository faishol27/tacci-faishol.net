require('dotenv').config()

// NPM Modules
const passport = require('passport')
const PassportLocal = require('passport-local')
const passportJWT = require('passport-jwt')
const path = require('path')

// Models
const User = require(path.join(process.env.APP_PATH, 'models/user'))

// Variables
var JWTstrategy = passportJWT.Strategy
var JWTextract = passportJWT.ExtractJwt
var JWTopts = {}
var cookieExtractor = function (req) {
  var token = null
  if (req && req.cookies) token = req.cookies['token']

  return token
}

JWTopts.jwtFromRequest = JWTextract.fromExtractors([cookieExtractor])
JWTopts.secretOrKey = String(process.env.JWT_SECRET)

// Configure passport strategy
passport.use(new PassportLocal(
  (username, password, callback) => {
    let potentialUser = {
      where: {
        username: username
      }
    }

    User.findOne(potentialUser).then((user) => {
      if (!user) return callback(null, false)
      if (!user.comparePassword(password)) return callback(null, false)
      return callback(null, user)
    }).catch(callback)
  }
))

passport.use(new JWTstrategy(JWTopts,
  (jwtPayload, callback) => {
    return callback(null, jwtPayload)
  }
))

module.exports = passport
