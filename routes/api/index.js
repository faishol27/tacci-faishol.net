require('dotenv').config()

// NPM Modules
const express = require('express')
const router = express.Router()

// Routing
router.use('/v1', require('./v1/index'))

module.exports = router
