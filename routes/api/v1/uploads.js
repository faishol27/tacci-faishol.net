require('dotenv').config()

// NPM Modules
const express = require('express')
const fs = require('fs')
const path = require('path')
const router = express.Router()

// Variables
const APP_PATH = process.env.APP_PATH

// Models
const UploadMdl = require(path.join(APP_PATH, '/models/upload'))

// Config
const passport = require(path.join(APP_PATH, '/configs/passport'))
const logger = require(path.resolve(APP_PATH, 'configs/winston'))

// Function
function failedResponse (res, statusCode, comment) {
  res.status(parseInt(statusCode))
  res.json({ 'status': 'failed', 'comment': comment })
}

function successResponse (res, apiRes) {
  res.status(200)
  res.json({ 'status': 'success', 'response': apiRes })
}

function isAuthenticate (req, res, next) {
  passport.authenticate('jwt', (err, user, info) => {
    if (err) {
      logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method, user: user.username })
      failedResponse(res, 500, 'internal server error')
    } else {
      var granted = user.level & (1 << 2)

      if (granted){
        req.user = user
        next()
      }else failedResponse(res, 401, 'unauthorized')
    }
  })(req, res, next)
}

// Routing
router.get('/uploads', isAuthenticate, (req, res, next) => {
  UploadMdl.findAll().then((uploadRows) => {
    successResponse(res, uploadRows)
  }).catch((err) => {
    logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method, user: req.user.username })
    failedResponse(res, 500, `internal server error`)
  })
})

router.post('/uploads', isAuthenticate, (req, res, next) => {
  UploadMdl.bulkCreate(req.body.data).then(() => {
    successResponse(res, 'successfully created')
  }).catch((err) => {
    logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method, user: req.user.username })
    failedResponse(res, 400, 'failed to insert new data')
  })
})

router.get('/upload/:id', isAuthenticate, (req, res, next) => {
  var uploadFilter = {
    where: {
      id: req.params['id']
    }
  }

  UploadMdl.findOne(uploadFilter).then((uploadRes) => {
    if (uploadRes == null) {
      failedResponse(res, 400, `file with id ${req.params['id']} not found`)
    } else {
      successResponse(res, uploadRes)
    }
  }).catch((err) => {
    logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method, user: req.user.username })
    failedResponse(res, 500, `internal server error`)
  })
})

router.delete('/upload/:id', isAuthenticate, (req, res, next) => {
  var uploadFilter = {
    where: {
      id: req.params['id']
    }
  }

  UploadMdl.findOne(uploadFilter).then((uploadRes) => {
    fs.unlink(uploadRes.getPath(), (err) => {
      if (err) {
        logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method, user: req.user.username })
      }

      UploadMdl.destroy(uploadFilter, { force: true }).then((deleteRes) => {
        if (deleteRes === 0) {
          failedResponse(res, 400, `file ${req.params['id']} failed to deleted`)
        } else {
          successResponse(res, `file ${req.params['id']} successfully deleted`)
        }
      }).catch((err) => {
        logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method, user: req.user.username })
        failedResponse(res, 500, `internal server error`)
      })
    })
  }).catch((err) => {
    logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method, user: req.user.username })
    failedResponse(res, 500, `internal server error`)
  })
})

module.exports = router
