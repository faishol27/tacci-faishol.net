require('dotenv').config()

// NPM Modules
const express = require('express')
const path = require('path')
const router = express.Router()
const Sequelize = require('sequelize')

// Variables
const APP_PATH = process.env.APP_PATH
const Op = Sequelize.Op

// Models
const TeamMdl = require(path.join(APP_PATH, '/models/team'))

// Config
const passport = require(path.join(APP_PATH, '/configs/passport'))
const logger = require(path.resolve(APP_PATH, 'configs/winston'))

// Function
function failedResponse (res, statusCode, comment) {
  res.status(parseInt(statusCode))
  res.json({ 'status': 'failed', 'comment': comment })
}

function successResponse (res, apiRes) {
  res.status(200)
  res.json({ 'status': 'success', 'response': apiRes })
}

function isAuthenticate (req, res, next) {
  passport.authenticate('jwt', (err, user, info) => {
    if (err) {
      logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method, user: user.username })
      failedResponse(res, 500, 'internal server error')
    } else {
      var granted = user.level & (1 << 2)

      if (granted){
        req.user = user
        next()
      }else failedResponse(res, 401, 'unauthorized')
    }
  })(req, res, next)
}

// Routing
router.get('/teams', (req, res, next) => {
  var teamFilter = {
    offset: 0,
    where: {}
  }

  if (req.query['from'] || req.query['to']) {
    if (req.query['from']) teamFilter.offset = parseInt(req.query['from']) - 1
    if (req.query['to']) teamFilter.limit = parseInt(req.query['to']) - parseInt(teamFilter.offset)
  } else if (req.query['id']) {
    teamFilter.where.id = {}
    teamFilter.where.id[Op.or] = req.query['id'].split(';')
  }

  TeamMdl.findAll(teamFilter).then((teamRows) => {
    var teamFind = []
    if (req.query['id']) teamFind = req.query['id'].split(';').map(v => parseInt(v))

    if ((req.query['from'] || req.query['to']) && teamFind.length > 0) {
      var tmp = []

      for (let i = 0; i < teamRows.length; i++) {
        var idx = teamFind.indexOf(teamRows[i].id)
        if (idx !== -1) {
          teamFind.splice(idx, 1)
          tmp.push(teamRows[i])
        }
      }

      teamRows = tmp
    } else if (req.query['id']) {
      for (let i = 0; i < teamRows.length; i++) {
        teamFind.splice(teamFind.indexOf(teamRows[i].id), 1)
      }
    }

    if (teamFind.length === 0) successResponse(res, teamRows)
    else {
      let failMsg = 'team id ' + teamFind.join(', ') + ' not found'
      failedResponse(res, 400, failMsg)
    }
  }).catch((err) => {
    logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method, user: req.user.username })
    failedResponse(res, 500, 'internal server error')
  })
})

router.get('/team/:id', (req, res, next) => {
  var teamFilter = {
    where: {
      id: req.params['id']
    }
  }

  TeamMdl.findOne(teamFilter).then((teamRes) => {
    if (teamRes == null) {
      failedResponse(res, 400, `team id ${req.params['id']} not found`)
    } else {
      successResponse(res, teamRes)
    }
  }).catch((err) => {
    logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method, user: req.user.username })
    failedResponse(res, 500, 'internal server error')
  })
})

router.put('/team/:id', isAuthenticate, (req, res, next) => {
  var teamFilter = {
    where: {
      id: req.params['id']
    }
  }
  var teamUpd = {}

  if (req.body['name']) teamUpd.name = req.body['name']
  if (req.body['school']) teamUpd.school = req.body['school']
  if (req.body['score']) teamUpd.score = req.body['score']
  if (req.body['last_boxType']) teamUpd.last_boxType = req.body['last_boxType']

  TeamMdl.update(teamUpd, teamFilter).then(() => {
    successResponse(res, `team id ${req.params['id']} successfully updated`)
  }).catch((err) => {
    logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method, user: req.user.username })
    failedResponse(res, 500, 'internal server error')
  })
})

module.exports = router
