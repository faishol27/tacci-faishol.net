require('dotenv').config()

// NPM Modules
const express = require('express')
const path = require('path')
const router = express.Router()

// Variables
const APP_PATH = process.env.APP_PATH

// Models
const ProblemMdl = require(path.join(APP_PATH, '/models/problem'))

// Config
const passport = require(path.join(APP_PATH, '/configs/passport'))
const logger = require(path.resolve(APP_PATH, 'configs/winston'))

// Function
function failedResponse (res, statusCode, comment) {
  res.status(parseInt(statusCode))
  res.json({ 'status': 'failed', 'comment': comment })
}

function successResponse (res, apiRes) {
  res.status(200)
  res.json({ 'status': 'success', 'response': apiRes })
}

function isAuthenticate (req, res, next) {
  passport.authenticate('jwt', (err, user, info) => {
    if (err) {
      logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method, user: user.username })
      failedResponse(res, 500, 'internal server error')
    } else {
      var granted = user.level & (1 << 1)

      if (granted){
        req.user = user
        next()
      }else failedResponse(res, 401, 'unauthorized')
    }
  })(req, res, next)
}

// Main navigation routing
router.get('/problems', isAuthenticate, (req, res, next) => {
  var getOffset = 0
  var getLimit = -1

  if (req.body['from']) getOffset = parseInt(req.body['from']) - 1
  if (req.body['to']) getLimit = parseInt(req.body['to']) - parseInt(getOffset)

  if (getLimit === -1) {
    ProblemMdl.findAll({ offset: getOffset }).then((probRows) => {
      successResponse(res, probRows)
    }).catch((err) => {
      logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method, user: req.user.username })
      failedResponse(res, 500, 'internal server error')
    })
  } else {
    ProblemMdl.findAll({ offset: getOffset, limit: getLimit }).then((probRows) => {
      successResponse(res, probRows)
    }).catch((err) => {
      logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method, user: req.user.username })
      failedResponse(res, 500, 'internal server error')
    })
  }
})

router.get('/problem/:id', isAuthenticate, (req, res, next) => {
  var probFilter = {
    where: {
      id: req.params['id']
    }
  }

  ProblemMdl.findOne(probFilter).then((probRow) => {
    if (probRow == null) { failedResponse(res, 400, `problem with id ${req.params['id']} not found`) } else {
      var retJSON = []
      retJSON.push(probRow)
      successResponse(res, retJSON)
    }
  }).catch((err) => {
    logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method, user: req.user.username })
    failedResponse(res, 500, 'internal server error')
  })
})

router.put('/problem/:id', isAuthenticate, (req, res, next) => {
  var probFilter = {
    where: {
      id: req.params['id']
    }
  }

  var probUpdate = {
    description: req.body['description'],
    answer: req.body['answer'],
    type: req.body['type'],
    category_id: req.body['category_id'],
    time_limit: req.body['time_limit']
  }

  var invalidForm = []
  for (let key in probUpdate) {
    if (probUpdate[key] == null) {
      invalidForm.push(key)
    }
  }

  if (invalidForm.length === 0) {
    ProblemMdl.update(probUpdate, probFilter).then(() => {
      successResponse(res, `problem ${req.params['id']} successfully updated`)
    }).catch((err) => {
      logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method, user: req.user.username })
      failedResponse(res, 500, 'internal server error')
    })
  } else {
    var msgError = invalidForm.join(', ') + ' cannot be empty'
    failedResponse(res, 400, msgError)
  }
})

module.exports = router
