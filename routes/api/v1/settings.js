require('dotenv').config()

// NPM Modules
const express = require('express')
const path = require('path')
const router = express.Router()

// Variables
const APP_PATH = process.env.APP_PATH

// Configs
const passport = require(path.join(APP_PATH, '/configs/passport'))
const logger = require(path.resolve(APP_PATH, 'configs/winston'))

// Models
const SettingMdl = require(path.join(APP_PATH, '/models/setting'))

// Function
function failedResponse (res, statusCode, comment) {
  res.status(parseInt(statusCode))
  res.json({ 'status': 'failed', 'comment': comment })
}

function successResponse (res, apiRes) {
  res.status(200)
  res.json({ 'status': 'success', 'response': apiRes })
}

function isAuthenticate (req, res, next) {
  passport.authenticate('jwt', (err, user, info) => {
    if (err) {
      logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method, user: user.username })
      failedResponse(res, 500, 'internal server error')
    } else {
      var granted = user.level & (1 << 2)

      if (granted){
        req.user = user
        next()
      }else failedResponse(res, 401, 'unauthorized')
    }
  })(req, res, next)
}

function bulkUpdateSettings (list, idx, failList, callback) {
  if (idx >= list.length) return callback(null, failList)

  var settingFilter = {
    where: {
      name: list[parseInt(idx)].name
    }
  }

  var settingUpdate = { value: list[parseInt(idx)].value }

  SettingMdl.update(settingUpdate, settingFilter).then(() => {
    bulkUpdateSettings(list, idx + 1, failList, callback)
  }).catch(err => callback(err, failList))
}

// Routing
router.get('/settings', (req, res, next) => {
  SettingMdl.findAll().then((settingRows) => {
    successResponse(res, settingRows)
  }).catch((err) => {
    logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method, user: req.user.username })
    failedResponse(res, 500, 'internal server error')
  })
})

router.put('/settings', isAuthenticate, (req, res, next) => {
  var listUpd = []
  for (let key in req.body) {
    listUpd.push({ name: key, value: req.body[key] })
  }

  bulkUpdateSettings(listUpd, 0, [], (err, fail) => {
    if (err) {
      logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method, user: req.user.username })
      failedResponse(res, 500, 'internal server error')
    } else {
      successResponse(res, 'All data successfully updated')
    }
  })
})

router.get('/setting/:name', (req, res, next) => {
  var settingFilter = {
    where: {
      name: req.params['name']
    }
  }

  SettingMdl.findOne(settingFilter).then((settingRows) => {
    if (settingRows == null) failedResponse(res, 400, `${req.params['name']} not found`)
    else successResponse(res, settingRows)
  }).catch((err) => {
    logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method, user: req.user.username })
    failedResponse(res, 500, 'internal server error')
  })
})

router.put('/setting/:name', isAuthenticate, (req, res, next) => {
  var settingFilter = {
    where: {
      name: req.params['name']
    }
  }
  var settingUpdate = { value: req.body['value'] }

  if (req.body['value']) {
    SettingMdl.update(settingUpdate, settingFilter).then(() => {
      successResponse(res, `${req.params['name']} value successfully updated`)
    }).catch((err) => {
      logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method, user: req.user.username })
      failedResponse(res, 500, 'internal server error')
    })
  } else { failedResponse(res, 400, `${req.params['name']} value cannot be empty`) }
})

module.exports = router
