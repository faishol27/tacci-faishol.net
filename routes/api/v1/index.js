require('dotenv').config()

// NPM Modules
const express = require('express')
const router = express.Router()

// Routing
router.use('', require('./problems'))
router.use('', require('./settings'))
router.use('', require('./teams'))
router.use('', require('./users'))
router.use('', require('./uploads'))
router.use('', require('./gamelogs'))

module.exports = router
