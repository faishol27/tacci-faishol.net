require('dotenv').config()

// NPM Modules
const express = require('express')
const path = require('path')
const router = express.Router()
const Sequelize = require('sequelize')

// Variables
const APP_PATH = process.env.APP_PATH
const Op = Sequelize.Op

// Models
const GamelogMdl = require(path.join(APP_PATH, '/models/gamelog'))

// Config
const passport = require(path.join(APP_PATH, '/configs/passport'))
const logger = require(path.resolve(APP_PATH, 'configs/winston'))

// Function
function failedResponse (res, statusCode, comment) {
  res.status(parseInt(statusCode))
  res.json({ 'status': 'failed', 'comment': comment })
}

function successResponse (res, apiRes) {
  res.status(200)
  res.json({ 'status': 'success', 'response': apiRes })
}

function isAuthenticate (req, res, next) {
  passport.authenticate('jwt', (err, user, info) => {
    if (err) {
      logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method, user: user.username })
      failedResponse(res, 500, 'internal server error')
    } else {
      var granted = user.level & (1 << 2)
      
      if (granted){
        req.user = user
        next()
      }else failedResponse(res, 401, 'unauthorized')
    }
  })(req, res, next)
}

// Routing
router.get('/gamelogs', (req, res, next) => {
  var dataFilter = { offset: 0, where: {} }

  if (req.query['active_log']) { dataFilter.where.active_log = req.query['active_log'] }

  GamelogMdl.findAll(dataFilter).then((rows) => {
    successResponse(res, rows)
  }).catch((err) => {
    logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method, user: req.user.username })
    failedResponse(res, 500, 'internal server error')
  })
})

router.post('/gamelogs', isAuthenticate, (req, res, next) => {
  GamelogMdl.bulkCreate(req.body.data).then(() => {
    successResponse(res, 'successfully created')
  }).catch((err) => {
    logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method, user: req.user.username })
    failedResponse(res, 400, 'failed to insert new data')
  })
})

router.put('/gamelogs', isAuthenticate, (req, res, next) => {
  var reqBody = req.body['data']
  var failUpd = reqBody.every(v => {
    var dataFilter = {
      where: { id: v.id }
    }
    var dataUpd = v
    delete dataUpd.id

    GamelogMdl.update(dataUpd, dataFilter).then(() => {
      return false
    }).catch(err => {
      logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method, user: req.user.username })
      return true
    })
  })

  if (failUpd === false) failedResponse(res, 400, 'some data failed to updated')
  else successResponse(res, 'successfully updated')
})

router.delete('/gamelogs', isAuthenticate, (req, res, next) => {
  if (req.body['id']) {
    // Delete gamelog with id = req.body['id']
    var dataID = req.body['id'].split(';')
    var dataFilter = {
      where: {
        id: {
          [Op.or]: dataID
        }
      }
    }

    GamelogMdl.destroy(dataFilter).then(successResponse(res, 'successfully deleted')).catch((err) => {
      logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method, user: req.user.username })
      failedResponse(res, 500, 'internal server error')
    })
  } else {
    // Delete all gamelog
    GamelogMdl.destroy({ truncate: true }).then(successResponse(res, 'successfully deleted')).catch((err) => {
      logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method, user: req.user.username })
      failedResponse(res, 500, 'internal server error')
    })
  }
})

router.get('/gamelog/:id', (req, res, next) => {
  var gamelogFilter = {
    where: { id: req.params['id'] }
  }

  GamelogMdl.findAll(gamelogFilter).then((rows) => {
    successResponse(res, rows)
  }).catch((err) => {
    logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method, user: req.user.username })
    failedResponse(res, 500, 'internal server error')
  })
})

router.put('/gamelog/:id', isAuthenticate, (req, res, next) => {
  var dataUpd = {}
  var dataFilter = {
    where: {
      id: req.params['id']
    }
  }

  for (var key in req.body) {
    dataUpd[key] = req.body[key]
  }

  GamelogMdl.update(dataUpd, dataFilter).then(successResponse(res, 'successfully updated')).catch((err) => {
    logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method, user: req.user.username })
    failedResponse(res, 500, 'internal server error')
  })
})

router.delete('/gamelog/:id', isAuthenticate, (req, res, next) => {
  var dataFilter = {
    where: {
      id: req.params['id']
    }
  }

  GamelogMdl.destroy(dataFilter).then(successResponse(res, 'successfully deleted')).catch((err) => {
    logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method, user: req.user.username })
    failedResponse(res, 500, 'internal server error')
  })
})

module.exports = router
