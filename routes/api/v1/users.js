require('dotenv').config()

// NPM Modules
const express = require('express')
const path = require('path')
const router = express.Router()
const Sequelize = require('sequelize')

// Variables
const APP_PATH = process.env.APP_PATH
const Op = Sequelize.Op

// Models
const UserMdl = require(path.join(APP_PATH, '/models/user'))

// Configs
const logger = require(path.resolve(APP_PATH, 'configs/winston'))

// Function
function failedResponse (res, statusCode, comment) {
  res.status(parseInt(statusCode))
  res.json({ 'status': 'failed', 'comment': comment })
}

function successResponse (res, apiRes) {
  res.status(200)
  res.json({ 'status': 'success', 'response': apiRes })
}

// Routing
router.get('/users', (req, res, next) => {
  var userFilter = {
    attributes: ['fullname', 'username', 'image'],
    where: {}
  }

  if (req.body['usernames']) {
    userFilter.where.username = {}
    userFilter.where.username[Op.or] = req.body['usernames'].split(';')
  }

  UserMdl.findAll(userFilter).then((userRows) => {
    if (req.body['usernames']) {
      var userFind = req.body['usernames'].split(';')
      for (let i = 0; i < userRows.length; i++) {
        userFind.splice(userFind.indexOf(userRows[i].username), 1)
      }

      if (userFind.length === 0) successResponse(res, userRows)
      else {
        var failMsg = 'User with username ' + userFind.join(', ') + ' not found'
        failedResponse(res, 400, failMsg)
      }
    } else { successResponse(res, userRows) }
  }).catch((err) => {
    logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method })
    failedResponse(res, 500, 'internal server error')
  })
})

module.exports = router
