require('dotenv').config()

// NPM Modules
const express = require('express')
const path = require('path')
const router = express.Router()

// Variables
const APP_PATH = String(process.env.APP_PATH)
const VIEWS_ADMIN = path.join(APP_PATH, '/views/admin')
const VIEWS_ERR = path.join(APP_PATH, '/views/error_pages')

// Configs
const passport = require(path.join(APP_PATH, '/configs/passport'))
const logger = require(path.join(APP_PATH, '/configs/winston'))

// Models
const SettingMdl = require(path.join(APP_PATH, '/models/setting'))
const TeamMdl = require(path.join(APP_PATH, '/models/team'))
const UploadMdl = require(path.join(APP_PATH, '/models/upload'))
const ProblemMdl = require(path.join(APP_PATH, '/models/problem'))

// Function
function isAuthenticated (req, res, next) {
  passport.authenticate('jwt',
    (err, user, info) => {
      if (err || !user) {
        if (err)   logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method, user: req.user.username })
        if (!user) console.log('User tidak ditemukan')

        let linkRedirect = encodeURIComponent(path.join('/admin', req.path))
        return res.redirect(`/admin/login?red=${linkRedirect}`)
      } else {
        var granted = user.level & (1 << 2)

        if (granted) {
          req.user = user
          next()
        } else {
          res.status(401)
          res.render(path.join(VIEWS_ERR, '401'))
        }
      }
    }
  )(req, res, next)
}

// Main Navigation Routing
router.get('/login', (req, res) => {
  passport.authenticate('jwt', (err, user, info) => {
    var linkRedirect = ''
    var fail = ''

    if (req.query['failure']) fail = req.query['failure']
    if (req.query['red']) linkRedirect = decodeURIComponent(req.query['red'])

    if (err) {
      logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method })
    } else if (user) {
      if (linkRedirect === '') return res.redirect('/admin')
      else return res.redirect(linkRedirect)
    }

    return res.render(path.join(VIEWS_ADMIN, 'login'), { red: linkRedirect, failure: fail })
  })(req, res)
})

router.get('/', isAuthenticated, (req, res, next) => {
  SettingMdl.findAll().then((settingRows) => {
    var settings = {}
    for (let i = 0; i < settingRows.length; i++) {
      if (settingRows[i].type === 'int') {
        settings[settingRows[i].name] = parseInt(settingRows[i].value)
      } else {
        settings[settingRows[i].name] = String(settingRows[i].value)
      }
    }

    TeamMdl.findAll({ limit: parseInt(settings.team_number) }).then((teamRows) => {
      let optRender = {
        user: req.user,
        settings: settings,
        teams: teamRows
      }

      res.render(path.join(VIEWS_ADMIN, 'dashboard'), optRender)
    }).catch((err) => {
      logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method, user: req.user.username })
      res.render(path.join(VIEWS_ERR, '500'))
    })
  }).catch((err) => {
    logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method, user: req.user.username })
    res.render(path.join(VIEWS_ERR, '500'))
  })
})

router.get('/settings', isAuthenticated, (req, res, next) => {
  SettingMdl.findAll().then((rows) => {
    var settings = {}
    for (let i = 0; i < rows.length; i++) {
      if (rows[i].type === 'int') {
        settings[rows[i].name] = parseInt(rows[i].value)
      } else {
        settings[rows[i].name] = String(rows[i].value)
      }
    }

    TeamMdl.findAll({ limit: parseInt(settings.team_number) }).then((teamRows) => {
      let optRender = {
        user: req.user,
        settings: settings,
        teams: teamRows
      }

      return res.render(path.join(VIEWS_ADMIN, 'setting'), optRender)
    })
  }).catch((err) => {
    logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method, user: req.user.username })
    res.render(path.join(VIEWS_ERR, '500'))
  })
})

router.get('/repositories', isAuthenticated, (req, res, next) => {
  UploadMdl.findAll().then((uploadRows) => {
    var uploads = []
    for (let i = 0; i < uploadRows.length; i++) {
      var tmp = {}

      tmp.id = uploadRows[i].id
      tmp.filename = uploadRows[i].realname
      tmp.fileURL = uploadRows[i].getURL(req.protocol)

      uploads.push(tmp)
    }

    return res.render(path.join(VIEWS_ADMIN, 'repository'), { user: req.user, uploads: uploads })
  }).catch((err) => {
      logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method, user: req.user.username })
    res.render(path.join(VIEWS_ERR, '500'))
  })
})

router.get('/problems', isAuthenticated, (req, res, next) => {
  var queryAttr = { name: ['board_column', 'board_row', 'problem_order'] }

  SettingMdl.findAll({ where: queryAttr }).then((settingRows) => {
    var settings = {}
    for (let i = 0; i < settingRows.length; i++) {
      if (settingRows[i].type === 'int') {
        settings[settingRows[i].name] = parseInt(settingRows[i].value)
      } else {
        settings[settingRows[i].name] = String(settingRows[i].value)
      }
    }
    var numberProblem = settings['board_column'] * settings['board_row']

    ProblemMdl.findAll({ limit: numberProblem }).then((problemRows) => {
      var problems = []
      for (let i = 0; i < problemRows.length; i++) {
        problems.push(problemRows[i])
      }

      res.render(path.join(VIEWS_ADMIN, 'problem'), {
        user: req.user,
        problems: problems,
        settings: settings
      })
    })
  }).catch((err) => {
      logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method, user: req.user.username })
    res.render(path.join(VIEWS_ERR, '500'))
  })
})

router.get('/problems/print', isAuthenticated, (req, res, next) => {
  SettingMdl.findAll().then((settingRows) => {
    var settings = {}
    for (let i = 0; i < settingRows.length; i++) {
      if (settingRows[i].type === 'int') {
        settings[settingRows[i].name] = parseInt(settingRows[i].value)
      } else {
        settings[settingRows[i].name] = String(settingRows[i].value)
      }
    }
    var numberProblem = settings['board_column'] * settings['board_row']

    ProblemMdl.findAll({ limit: numberProblem }).then((problemRows) => {
      var problems = []
      for (let i = 0; i < problemRows.length; i++) {
        problems.push(problemRows[i])
      }

      res.render(path.join(VIEWS_ADMIN, 'problem_print'), {
        user: req.user,
        problems: problems,
        settings: settings
      })
    })
  }).catch((err) => {
      logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method, user: req.user.username })
    res.render(path.join(VIEWS_ERR, '500'))
  })
})

router.get('/problem/:id/edit', isAuthenticated, (req, res, next) => {
  var probFilter = {
    where: {
      id: req.params['id']
    }
  }

  ProblemMdl.findOne(probFilter).then((problemRes) => {
    res.render(path.join(VIEWS_ADMIN, 'problem_edit'), {
      user: req.user,
      problem: problemRes
    })
  }).catch((err) => {
      logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method, user: req.user.username })
    res.status(500)
    res.render(path.join(VIEWS_ERR, '500'))
  })
})

router.get('/about', isAuthenticated, (req, res, next) => {
  res.render(path.join(VIEWS_ADMIN, 'about'), { user: req.user })
})

router.get('/profile', isAuthenticated, (req, res, next) => {
  res.render(path.join(VIEWS_ADMIN, 'profile'), { user: req.user })
})

// Tutorial Routing
router.get('/tutorial', isAuthenticated, (req, res, next) => {
  res.redirect('/admin/tutorial/control')
})

router.get('/tutorial/control', isAuthenticated, (req, res, next) => {
  res.render(path.join(VIEWS_ADMIN, 'tutorial-control'), { user: req.user })
})

router.get('/tutorial/markdown', isAuthenticated, (req, res, next) => {
  res.render(path.join(VIEWS_ADMIN, 'tutorial-markdown'), { user: req.user })
})

module.exports = router
