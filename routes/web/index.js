require('dotenv').config()

// NPM Modules
const express = require('express')
const router = express.Router()

// Routing
router.use('/admin', require('./admin'))
router.use('/', require('./public'))

module.exports = router
