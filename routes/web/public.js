require('dotenv').config()

// NPM Modules
const express = require('express')
const path = require('path')
const router = express.Router()

// Variables
let APP_PATH = process.env.APP_PATH

// Models
const SettingMdl = require(path.resolve(APP_PATH, 'models/setting'))
const TeamMdl = require(path.resolve(APP_PATH, 'models/team'))
const ProblemMdl = require(path.resolve(APP_PATH, 'models/problem'))

// Configs
const logger = require(path.resolve(APP_PATH, 'configs/winston'))

// Routing
router.get('/', async (req, res, next) => {
  try{
    var settings = await SettingMdl.findAll().then(settingRows => {
      var ret = {}
      for (let i = 0; i < settingRows.length; i++) {
        if (settingRows[i].type === 'int') {
          ret[settingRows[i].name] = parseInt(settingRows[i].value)
        } else {
          ret[settingRows[i].name] = String(settingRows[i].value)
        }
      }

      return ret
    }).catch(err => { throw err })

    var teams = await TeamMdl.findAll({limit: parseInt(settings['team_number'])}).then(teamRows => teamRows)
      .catch(err => { throw err })

    var probLimit = settings['board_row'] * settings['board_column']
    var problems = await ProblemMdl.findAll({ limit: probLimit }).then(probRows => probRows)
      .catch(err => { throw err })

  }catch(err){
    logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method })
    return res.status(500)

  }finally{
    res.render(path.join(APP_PATH, '/views/public'), {
      title: settings['title'],
      settings: settings,
      teams: teams,
      problems: problems
    })
  }
})

module.exports = router
