require('dotenv').config()

// NPM Modules
const express = require('express')
const formidable = require('formidable')
const fs = require('fs')
const path = require('path')
const randomstring = require('randomstring')
const request = require('request')
const router = express.Router()

// Variables
const APP_PATH = process.env.APP_PATH
const APP_HOST = process.env.APP_HOST

// Configs
const passport = require(path.join(APP_PATH, '/configs/passport'))
const logger = require(path.resolve(APP_PATH, 'configs/winston'))

// Models
const UploadMdl = require(path.join(APP_PATH, '/models/upload'))

// Function
function isAuthenticate (req, res, next) {
  passport.authenticate('jwt', { session: false }, (err, user, info) => {
    if (err) {
      logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method, user: user.username })
      res.status(500)
      res.render(path.join(APP_PATH, '/views/error_pages/500'))
    } else if (!user) {
      logger.error({ message: 'not authenticated', target: req.url, ip: req.ip, method: req.method, user: user.username })
      res.status(401)
      res.render(path.join(APP_PATH, '/views/error_pages/401'))
    } else {
      let granted = user.level & (1 << 2)
      if (granted > 0) {
        req.user = user
        next()
      } else {
        logger.error({ message: 'not granted', target: req.url, ip: req.ip, method: req.method, user: user.username })
        res.status(403)
        res.render(path.join(APP_PATH, '/views/error_pages/403'))
      }
    }
  })(req, res, next)
}

// Routing
router.get('/auth', (req, res, next) => {
  res.clearCookie('token')
  res.redirect('/')
})

router.post('/auth', (req, res, next) => {
  passport.authenticate('jwt', { session: false }, (err, user, info) => {
    if (err || !user) {
      passport.authenticate('local', { session: false },
        (err, user, info) => {
          var linkRedirect = ''
          if (req.body['red']) { linkRedirect = encodeURIComponent(req.body['red']) }

          if (err) {
            logger.error({ message: 'not authenticated', target: req.url, ip: req.ip, method: req.method})
            if (linkRedirect === '') return res.redirect(`/admin/login?failure=system`)
            else return res.redirect(`/admin/login?failure=system&red=${linkRedirect}`)
          } else if (!user) {
            if (linkRedirect === '') return res.redirect(`/admin/login?failure=login`)
            else return res.redirect(`/admin/login?failure=login&red=${linkRedirect}`)
          } else {
            res.cookie('token', user.generateJWT())
            if (linkRedirect === '') return res.redirect('/admin')
            else return res.redirect(decodeURIComponent(linkRedirect))
          }
        }
      )(req, res, next)
    } else {
      var linkRedirect = ''
      if (req.body['red']) { linkRedirect = encodeURIComponent(req.body['red']) }

      if (linkRedirect === '') return res.redirect('/admin')
      else return res.redirect(decodeURIComponent(linkRedirect))
    }
  })(req, res, next)
})

router.post('/uploads', isAuthenticate, (req, res, next) => {
  var fnames = req.body['filenames'].split(';')
  var tmpdir = path.join(APP_PATH, 'tmp')
  
  // Check if all file exists
  fnames.forEach(v => {
    if (!fs.existsSync(path.join(tmpdir, v))) {
      return res.status(400)
    }
  })

  // Move all files to uploads directory
  var dataUpload = fnames.map(v => {
    var wkt = v.split('_')[0]

    var realname = v.split('_')[1]
    
    var newdir = path.join(APP_PATH, '/public/uploads')
    var newname = wkt + randomstring.generate({ charset: 'alphanumeric', length: 16 })
    var oldPath = path.join(tmpdir, v)

    var newPath = path.join(newdir, newname)

    fs.renameSync(oldPath, newPath)
    return { user_id: req.user.id, filename: newname, realname: realname }
  })
  
  UploadMdl.bulkCreate(dataUpload).then(() => {
    res.send(200)
  }).catch((err) => {
    logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method, user: req.user.username })
    res.send(500)
  })
})

router.post('/filepond', isAuthenticate, (req, res, next) => {
  var tmpdir = path.join(APP_PATH, 'tmp')
  var form = new formidable.IncomingForm()
  if (!fs.existsSync(tmpdir)) {
    fs.mkdirSync(tmpdir)
  }

  form.multiples = true
  form.uploadDir = tmpdir

  form.parse(req, (err, fields, files) => {
    if (err) {
      logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method, user: req.user.username })
      return res.send(500)
    }

    if (files['file[]']) {
      var filecode = `${Date.now()}_${files['file[]'].name}`
      var newFilepath = path.join(tmpdir, filecode)

      fs.rename(files['file[]'].path, newFilepath, (err) => {
        if (err) {
            logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method, user: req.user.username })
          res.status(500)
        } else res.send(filecode)
      })
    }
  })
})

router.delete('/filepond', isAuthenticate, (req, res, next) => {
  var tmpdir = path.join(APP_PATH, 'tmp')
  var filecode = req.body
  var fullPath = path.join(tmpdir, filecode)

  fs.unlink(fullPath, (err) => {
    if (err) {
      logger.error({ message: err.message, target: req.url, ip: req.ip, method: req.method, user: req.user.username })
      res.status(500)
      res.send('internal server error')
    } else {
      res.status(200)
      res.send('success')
    }
  })
})

module.exports = router
