require('dotenv').config()

// NPM Modules
const path = require('path')
const Sequelize = require('sequelize')

// Configs
const sequelize = require(path.join(process.env.APP_PATH, '/configs/sequelize'))

// Model definition
const Setting = sequelize.define('Settings', {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    notNull: true
  },
  name: {
    type: Sequelize.STRING(100),
    notNull: true,
    unique: true
  },
  data_type: {
    type: Sequelize.STRING(25),
    notNull: true
  },
  value: {
    type: Sequelize.STRING,
    length: 100,
    notNull: true
  },
  comment: {
    type: Sequelize.STRING
  },
  createdAt: {
    allowNull: false,
    type: Sequelize.DATE,
    defaultValue: Sequelize.fn('NOW')
  },
  updatedAt: {
    allowNull: false,
    type: Sequelize.DATE,
    defaultValue: Sequelize.fn('NOW')
  }
})

module.exports = Setting
