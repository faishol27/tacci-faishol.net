require('dotenv').config()

// NPM Modules
const path = require('path')
const Sequelize = require('sequelize')

// Configs
const sequelize = require(path.join(process.env.APP_PATH, '/configs/sequelize'))

// Model definition
const UploadModel = sequelize.define('Uploads', {
  id: {
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: Sequelize.INTEGER
  },
  user_id: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  filename: {
    type: Sequelize.STRING,
    unique: true,
    allowNull: false
  },
  realname: {
    type: Sequelize.STRING,
    allowNull: false
  },
  createdAt: {
    allowNull: false,
    type: Sequelize.DATE,
    defaultValue: Sequelize.fn('NOW')
  },
  updatedAt: {
    allowNull: false,
    type: Sequelize.DATE,
    defaultValue: Sequelize.fn('NOW')
  }
})

UploadModel.prototype.getURL = function (protocol) {
  var fullHostname = `${process.env.APP_HOST}/`

  var fileURL = `/uploads/${this.filename}`
  var fullURL = `${protocol}://${path.join(fullHostname, fileURL)}`

  return fullURL
}

UploadModel.prototype.getPath = function () {
  let fullDir = path.join(process.env.APP_PATH, 'public/uploads')
  let fullPath = path.join(fullDir, this.filename)
  return fullPath
}

module.exports = UploadModel
