require('dotenv').config()

// NPM Modules
const path = require('path')
const Sequelize = require('sequelize')

// Configs
const sequelize = require(path.join(process.env.APP_PATH, '/configs/sequelize'))

// Model definition
const Category = sequelize.define('Categories', {
  id: {
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: Sequelize.INTEGER
  },
  name: {
    type: Sequelize.STRING,
    allowNull: false,
    unique: true
  },
  icon: {
    type: Sequelize.STRING
  },
  createdAt: {
    allowNull: false,
    type: Sequelize.DATE,
    defaultValue: Sequelize.fn('NOW')
  },
  updatedAt: {
    allowNull: false,
    type: Sequelize.DATE,
    defaultValue: Sequelize.fn('NOW')
  }
})

module.exports = Category
