'use strict'
require('dotenv').config()

// NPM Modules
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const path = require('path')
const randomstring = require('randomstring')
const Sequelize = require('sequelize')

// Configs
const sequelize = require(path.join(process.env.APP_PATH, '/configs/sequelize'))

// Model definition
const User = sequelize.define('Users', {
  id: {
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: Sequelize.INTEGER
  },
  fullname: {
    type: Sequelize.STRING,
    allowNull: false
  },
  email: {
    type: Sequelize.STRING,
    unique: true,
    allowNull: false,
    validate: {
      isEmail: true
    }
  },
  username: {
    type: Sequelize.STRING,
    unique: true,
    allowNull: false
  },
  password: {
    type: Sequelize.STRING,
    allowNull: false
  },
  level: {
    type: Sequelize.INTEGER,
    defaultValue: 1
  },
  image: {
    type: Sequelize.STRING,
    allowNull: false,
    defaultValue: '/img/cavatar.png'
  },
  createdAt: {
    allowNull: false,
    type: Sequelize.DATE,
    defaultValue: Sequelize.fn('NOW')
  },
  updatedAt: {
    allowNull: false,
    type: Sequelize.DATE,
    defaultValue: Sequelize.fn('NOW')
  }
})

User.addHook('beforeValidate', 'hashPassword', (user, opt) => {
  if (user.changed('password')) user.password = bcrypt.hashSync(user.password, 13)
})

User.prototype.comparePassword = function (password) {
  return bcrypt.compareSync(password, this.password)
}

User.prototype.generateJWT = function () {
  let jwtSecret = String(process.env.JWT_SECRET)
  let jwtPayload = {}

  jwtPayload.id = this.id
  jwtPayload.fullname = this.fullname
  jwtPayload.email = this.email
  jwtPayload.username = this.username
  jwtPayload.level = this.level
  jwtPayload.image = this.image

  jwtPayload.xsrf_token = randomstring.generate(64)

  return jwt.sign(jwtPayload, jwtSecret, { expiresIn: '3h' })
}

module.exports = User
