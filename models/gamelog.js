require('dotenv').config()

// NPM Modules
const path = require('path')
const Sequelize = require('sequelize')

// Configs
const sequelize = require(path.join(process.env.APP_PATH, '/configs/sequelize'))

// Model definition
const Gamelog = sequelize.define('Gamelogs', {
  id: {
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: Sequelize.INTEGER
  },
  user_id: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  description: {
    type: Sequelize.TEXT
  },
  json_data: {
    type: Sequelize.TEXT
  },
  active_log: {
    type: Sequelize.INTEGER,
    defaultValue: 1,
    allowNull: false
  },
  createdAt: {
    allowNull: false,
    type: Sequelize.DATE,
    defaultValue: Sequelize.fn('NOW')
  },
  updatedAt: {
    allowNull: false,
    type: Sequelize.DATE,
    defaultValue: Sequelize.fn('NOW')
  }
})

module.exports = Gamelog
