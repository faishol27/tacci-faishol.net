require('dotenv').config()

// NPM Modules
const path = require('path')
const Sequelize = require('sequelize')

// Configs
const sequelize = require(path.join(process.env.APP_PATH, '/configs/sequelize'))

// Model definition
const Team = sequelize.define('Teams', {
  id: {
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: Sequelize.INTEGER
  },
  name: {
    type: Sequelize.STRING
  },
  school: {
    type: Sequelize.STRING
  },
  score: {
    type: Sequelize.INTEGER,
    defaultValue: 0
  },
  last_boxType: {
    type: Sequelize.INTEGER,
    defaultValue: 0
  },
  createdAt: {
    allowNull: false,
    type: Sequelize.DATE,
    defaultValue: Sequelize.fn('NOW')
  },
  updatedAt: {
    allowNull: false,
    type: Sequelize.DATE,
    defaultValue: Sequelize.fn('NOW')
  }
})

module.exports = Team
