require('dotenv').config()

// NPM Modules
const path = require('path')
const Sequelize = require('sequelize')

// Configs
const sequelize = require(path.join(process.env.APP_PATH, '/configs/sequelize'))

// Model definition
const Problem = sequelize.define('Problems', {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    notNull: true
  },
  description: {
    type: Sequelize.TEXT
  },
  answer: {
    type: Sequelize.TEXT
  },
  category_id: {
    type: Sequelize.INTEGER,
    allowNull: false,
    defaultValue: 0
  },
  type: {
    type: Sequelize.INTEGER,
    defaultValue: 0,
    allowNull: true,
    comments: '[empty, ambil_alih, double_point, extra_box, sedekah_point, sedekah_box, skipped]'
  },
  time_limit: {
    type: Sequelize.INTEGER,
    defaultValue: 0,
    allowNull: true,
    comments: 'in second(s)'
  },
  createdAt: {
    allowNull: false,
    type: Sequelize.DATE,
    defaultValue: Sequelize.fn('NOW')
  },
  updatedAt: {
    allowNull: false,
    type: Sequelize.DATE,
    defaultValue: Sequelize.fn('NOW')
  }
})

module.exports = Problem
