require('dotenv').config()

// ENV variables
process.env.APP_PATH = __dirname
if (process.env.APP_HOST === 'localhost') {
  process.env.APP_HOST = `${process.env.APP_HOST}:${process.env.APP_PORT}`
}

// NPM Modules
const path = require('path')

// Configs
const app = require(path.join(process.env.APP_PATH, '/configs/express'))
const server = require(path.join(process.env.APP_PATH, '/configs/socket-io'))

// Variables
const APP_DIR = String(process.env.APP_PATH)
const APP_PORT = (process.env.APP_PORT || 8080)

// Routes
const webRoutes = require(path.join(APP_DIR, '/routes/web/index'))
const apiRoutes = require(path.join(APP_DIR, '/routes/api/index'))
const processRoutes = require(path.join(APP_DIR, '/routes/process/index'))
app.use('/', webRoutes)
app.use('/api', apiRoutes)
app.use('/process', processRoutes)

// Run the server
server.listen(APP_PORT, () => {
  console.log(`Running in http://localhost:${APP_PORT}/ and Have Fun!`)
})
