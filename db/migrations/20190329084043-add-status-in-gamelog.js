'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {    
        return queryInterface.addColumn('Gamelogs', 'active_log', {
            type: Sequelize.INTEGER,
            defaultValue: 1,
            allowNull: false
        }, { after: 'json_data' });
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.removeColumn('Gamelogs', 'active_log', {});
    }
};
