'use strict';
module.exports = (sequelize, DataTypes) => {
  const Setting = sequelize.define('Setting', {
    name: DataTypes.STRING,
    data_type: DataTypes.STRING,
    value: DataTypes.STRING,
    comment: DataTypes.STRING
  }, {});
  Setting.associate = function(models) {
    // associations can be defined here
  };
  return Setting;
};