'use strict';
module.exports = (sequelize, DataTypes) => {
  const Gamelog = sequelize.define('Gamelog', {
    user_id: DataTypes.INTEGER,
    description: DataTypes.TEXT,
    json_data: DataTypes.TEXT
  }, {});
  Gamelog.associate = function(models) {
    // associations can be defined here
  };
  return Gamelog;
};