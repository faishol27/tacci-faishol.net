'use strict';
module.exports = (sequelize, DataTypes) => {
  const Problem = sequelize.define('Problem', {
    description: DataTypes.TEXT,
    answer: DataTypes.TEXT,
    category_id: DataTypes.INTEGER,
    type: DataTypes.INTEGER,
    time_limit: DataTypes.INTEGER
  }, {});
  Problem.associate = function(models) {
    // associations can be defined here
  };
  return Problem;
};