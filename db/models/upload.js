'use strict';
module.exports = (sequelize, DataTypes) => {
  const Upload = sequelize.define('Upload', {
    user_id: DataTypes.INTEGER,
    filename: DataTypes.STRING,
    realname: DataTypes.STRING
  }, {});
  Upload.associate = function(models) {
    // associations can be defined here
  };
  return Upload;
};