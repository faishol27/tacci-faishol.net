'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        var defaultRows = [];
        defaultRows.push({name: "logo", data_type: "string", value: ""});
        defaultRows.push({name: "title", data_type: "string", value: "Tacci"});
        defaultRows.push({name: "subtitle", data_type: "string", value: ""});
        defaultRows.push({name: "background_box", data_type: "string", value: ""});
        defaultRows.push({name: "background_board", data_type: "string", value: ""});
        defaultRows.push({name: "time_limit", data_type: "int", value: "900000", comment: "in milisecond(s)"});
        defaultRows.push({name: "team_number", data_type: "int", value: "3"});
        defaultRows.push({name: "team_order", data_type: "string", value: "1,2,3"});
        defaultRows.push({name: "score_bonus", data_type: "int", value: "300"});
        defaultRows.push({name: "score_zonk", data_type: "int", value: "-100"});
        defaultRows.push({name: "score_correct", data_type: "int", value: "100"});
        defaultRows.push({name: "score_wrong", data_type: "int", value: "-25"});
        defaultRows.push({name: "board_column", data_type: "string", value: "5"});
        defaultRows.push({name: "board_row", data_type: "string", value: "5"});
        defaultRows.push({name: "problem_order", data_type: "string", value: "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25"});
        defaultRows.push({name: "game_active", data_type: "int", value: "0"});
        
        for(let i = 0; i < defaultRows.length; i++){
            defaultRows[i].createdAt = new Date();
            defaultRows[i].updatedAt = new Date();
        }

        return queryInterface.bulkInsert('Settings', defaultRows, {});
    },

    down: (queryInterface, Sequelize) => {
        var defaultRows = [ "logo", "title", "subtitle", "background_box", "background_board", "time_limit",
                            "team_number", "team_order", "score_bonus", "score_zonk", "score_correct", "score_wrong",
                            "board_column", "board_row", "problem_order", "game_active"];

        return queryInterface.bulkDelete('Settings', { name: defaultRows }, {});
    }
};
