'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        var defaultRows = [];
        for(let i = 0; i < 100; i++){
            defaultRows.push({
                description: "",
                answer: "",
                createdAt: new Date(),
                updatedAt: new Date()
            })
        }

        return queryInterface.bulkInsert('Problems', defaultRows, {});
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('Problems', null, {});
    }
};
