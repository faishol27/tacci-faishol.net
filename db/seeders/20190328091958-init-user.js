'use strict';

// NPM Modules
const bcrypt = require('bcryptjs');

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert('Users', [{
            fullname: "Admin",
            email: "admin@admin",
            username: "admin",
            password: bcrypt.hashSync("admin", 13),
            level: 15,
            createdAt: new Date(),
            updatedAt: new Date()
        }], {});
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('Users', { username: "admin" }, {});
    }
};
