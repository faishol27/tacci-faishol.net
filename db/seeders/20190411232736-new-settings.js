'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        var defaultRows = [];
        defaultRows.push({name: "box_fontcolor", data_type: "string", value: "#000000"});

        for(let i = 0; i < defaultRows.length; i++){
            defaultRows[i].createdAt = new Date();
            defaultRows[i].updatedAt = new Date();
        }

        return queryInterface.bulkInsert('Settings', defaultRows, {});
    },

    down: (queryInterface, Sequelize) => {
        var defaultRows = [ "box_fontcolor"];

        return queryInterface.bulkDelete('Settings', { name: defaultRows }, {});
    }
};
