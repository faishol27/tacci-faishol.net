'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        var defaultRows = [];
        for(let i = 0; i < 10; i++){
            defaultRows.push({
                name: "",
                school: "",
                createdAt: new Date(),
                updatedAt: new Date()
            })
        }

        return queryInterface.bulkInsert('Teams', defaultRows, {});
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('Teams', null, {});
    }
};
